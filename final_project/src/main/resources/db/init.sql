DROP TABLE IF EXISTS CATEGORY;
CREATE TABLE CATEGORY (
    id integer IDENTITY NOT NULL PRIMARY KEY,
    name character varying(255) NOT NULL,
);

DROP TABLE IF EXISTS USER;
CREATE TABLE USER (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    role_id character varying(8) NOT NULL,
    email character varying(100) NOT NULL,
    password character varying(60),
);

DROP TABLE IF EXISTS TICKET;
CREATE TABLE TICKET (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    name character varying(100),
    description character varying(500),
    created_on SMALLDATETIME NOT NULL,
/*    desired_resolution_date SMALLDATETIME default NULL,*/
    desired_resolution_date DATE default NULL,
    assignee_id int,
    owner_id int,
    state_id character varying(32) NOT NULL,
    category_id int,
    urgency_id  character varying(8),
    approver_id int,
    foreign key (assignee_id) references USER(id),
    foreign key (owner_id) references USER(id),
    foreign key (approver_id) references USER(id),
    foreign key (category_id) references CATEGORY(id)
);


DROP TABLE IF EXISTS FEEDBACK;
CREATE TABLE FEEDBACK (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    user_id integer NOT NULL,
    rate integer NOT NULL,
    date SMALLDATETIME NOT NULL,
    text character varying(500),
    ticket_id integer NOT NULL,
    foreign key (user_id ) references USER(id)
);

DROP TABLE IF EXISTS ATTACHMENT;
CREATE TABLE ATTACHMENT (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    blob BLOB NOT NULL,
    ticket_id int,
    name character varying(255) NOT NULL,
    foreign key (ticket_id ) references TICKET(id),
);


DROP TABLE IF EXISTS HISTORY;
CREATE TABLE HISTORY (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    ticket_id int NOT NULL,
    date SMALLDATETIME NOT NULL,
    action character varying(255) NOT NULL,
    user_id int NOT NULL,
    description character varying(255) NOT NULL,
    foreign key (ticket_id ) references TICKET(id),
    foreign key (user_id ) references USER(id)
);



DROP TABLE IF EXISTS COMMENT;
CREATE TABLE COMMENT (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    user_id int NOT NULL,
    date SMALLDATETIME NOT NULL,
    text character varying(500) NOT NULL,
    ticket_id int NOT NULL,
    foreign key (ticket_id ) references TICKET(id),
    foreign key (user_id ) references USER(id)

);



INSERT INTO USER (first_name, last_name, role_id, email, password) values
('emp1FirstName', 'emp1LastName', 'EMPLOYEE', 'user1_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq'),
('emp2FirstName', 'emp1LastName', 'EMPLOYEE', 'user2_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq'),
('mgr1FirstName', 'mgr1LastName', 'MANAGER', 'manager1_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq'),
('mgr2FirstName', 'mgr2LastName', 'MANAGER', 'manager2_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq'),
('eng1FirstName', 'eng1LastName', 'ENGINEER', 'engineer1_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq'),
('eng2FirstName', 'eng2LastName', 'ENGINEER', 'engineer2_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq');

insert into CATEGORY(NAME)  values 
('Application & Services'),
('Benefits & Paper Work'),
('Hardware & Software'),
('People Management'),
('Security & Access'),
('Workplaces & Facilities');



INSERT INTO TICKET (name, description,created_on,desired_resolution_date,assignee_id,owner_id,state_id,category_id,
    urgency_id,approver_id) values
('ticket name 1', 'ticket descr 1', '2019-10-17 18:47:52', '2019-09-18', 1, 1, 'DRAFT', 3, 'LOW', 1);
/*
('ticket name 2', 'ticket descr 2', '2019-09-17 18:47:52', '2019-09-18', 5, 1, 'DRAFT', 1, 'AVERAGE', 3),
('ticket name 3', 'ticket descr 3', '2019-09-17 18:47:52', '2019-09-18', 5, 1, 'DONE', 1, 'HIGH', 3),
('ticket name 4', 'ticket descr 4', '2019-09-17 18:47:52', '2019-09-20', 5, 1, 'APPROVED', 1, 'CRITICAL', 3),
('ticket name 5', 'ticket descr 5', '2019-09-17 18:47:52', '2019-09-19', 4, 1, 'DECLINED', 1, 'LOW', 3),
('mgr1 ticket', 'ticket descr 2', '2019-09-17 18:47:52', '2019-09-18', 1, 1, 'CANCELLED', 1, 'AVERAGE', 3),
('mgr2 ticket', 'ticket descr 2', '2019-09-17 18:47:52', '2019-09-18', 1, 1, 'DONE', 2, 'HIGH', 1),
('eng1 ticket', 'ticket descr 2', '2019-09-17 18:47:52', '2019-09-18', 1, 1, 'NEW', 1, 'AVERAGE', 5),
('ticket name 5b', 'ticket descr 5', '2019-09-17 18:47:52', '2019-09-19', 4, 1, 'IN_PROGRESS', 1, 'LOW', 3),
('ticket name 5c', 'ticket descr 5c', '2019-09-17 18:47:52', '2019-09-19', 4, 1, 'NEW', 1, 'AVERAGE', 3),
('ticket name 5d', 'ticket descr 5', '2019-09-17 18:47:52', '2019-09-19', 4, 1, 'DECLINED', 1, 'HIGH', 2),
('ticket name 5e', 'ticket descr 5', '2019-09-17 18:47:52', '2019-09-19', 4, 1, 'DECLINED', 1, 'AVERAGE', 2);
 
*/



insert into ATTACHMENT (blob, ticket_id, name) values
(x'89504E', 1, 'file1.pdf');
insert into ATTACHMENT (blob, ticket_id, name) values
(x'89504E', 1, 'file2.pdf');

insert into history (ticket_id, date, action, user_id, description) values
(1,'2019-09-01 18:47:52', 'action1', 1, 'desc1'),
(1,'2019-09-02 18:47:52', 'action2', 1, 'desc2'),
(1,'2019-09-03 18:47:52', 'action3', 1, 'desc3'),
(1,'2019-09-04 18:47:52', 'action4', 1, 'desc4'),
(1,'2019-09-05 18:47:52', 'action5', 1, 'desc5'),
(1,'2019-09-06 18:47:52', 'action6', 1, 'desc6'),
(1,'2019-09-07 18:47:52', 'action7', 1, 'desc7');

insert into COMMENT (user_id, date, text, ticket_id) values
(1,'2019-09-01 18:47:52','comment1',1),
(1,'2019-09-02 18:47:52','comment2',1),
(1,'2019-09-03 18:47:52','comment3',1),
(1,'2019-09-04 18:47:52','comment4',1),
(1,'2019-09-05 18:47:52','comment5',1),
(1,'2019-09-06 18:47:52','comment6',1),
(1,'2019-09-07 18:47:52','comment7',1),
(1,'2019-09-08 18:47:52','comment8',1);