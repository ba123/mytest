package epam.training.helpdesk.domain;

import java.util.Objects;

public class OperationResult {
    private Boolean operationComplete;
    private Integer id;

    public OperationResult() {
    }

    public OperationResult(Boolean operationComplete, Integer id) {
        this.operationComplete = operationComplete;
        this.id = id;
    }

    public Boolean isOperationComplete() {
        return operationComplete;
    }

    public void setOperationComplete(Boolean operationComplete) {
        this.operationComplete = operationComplete;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OperationResult that = (OperationResult) o;
        return operationComplete == that.operationComplete &&
                id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(operationComplete, id);
    }
}
