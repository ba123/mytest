package epam.training.helpdesk.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Feedback entity.
 */
@Entity
@Table(name = "Feedback")
public class Feedback {
    /**
     * Feedback id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    /**
     * Feedback author.
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    private User feedbackAuthor;


    /**
     * Feedback rating.
     */
    @Column(name = "rate")
    @NotNull
    private Integer rate;

    /**
     * Geedback date.
     */
    @Column(name = "date")
    @NotNull
    private Timestamp date;

    /**
     * Feedback text.
     */
    @Column(name = "text")
    private String text;

    /**
     * Ticket that was rated with feedback.
     */
    @OneToOne
    @JoinColumn(name ="ticket_id" , referencedColumnName ="id")
    @NotNull
    private Ticket feedbackedTicket;

    public Feedback() {
    }

    public Feedback(Integer rate, String text) {
        this.rate = rate;
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getFeedbackAuthor() {
        return feedbackAuthor;
    }

    public void setFeedbackAuthor(User feedbackAuthor) {
        this.feedbackAuthor = feedbackAuthor;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Ticket getFeedbackedTicket() {
        return feedbackedTicket;
    }

    public void setFeedbackedTicket(Ticket feedbackedTicket) {
        this.feedbackedTicket = feedbackedTicket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Feedback feedback = (Feedback) o;
        return Objects.equals(id, feedback.id)
                && Objects.equals(feedbackAuthor, feedback.feedbackAuthor)
                && Objects.equals(rate, feedback.rate)
                && Objects.equals(date, feedback.date)
                && Objects.equals(text, feedback.text)
                && Objects.equals(feedbackedTicket, feedback.feedbackedTicket);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, feedbackAuthor, rate, date, text, feedbackedTicket);
    }
}
