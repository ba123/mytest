package epam.training.helpdesk.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Comment entity.
 */

@Entity
@Table(name = "Comment")
public class Comment {
    /**
     * Comment id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Comment's author.
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    private User commentator;


    /**
     * Comment text.
     */
    @Column(name = "text")
    @NotNull
    private String text;

    /**
     * Comment date.
     */

    @Column(name = "date")
    @NotNull
    private Timestamp date;

    /**
     * Ticket that comment was applied to.
     */

    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket commentedTicket;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getCommentator() {
        return commentator;
    }

    public void setCommentator(User commentator) {
        this.commentator = commentator;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Ticket getCommentedTicket() {
        return commentedTicket;
    }

    public void setCommentedTicket(Ticket commentedTicket) {
        this.commentedTicket = commentedTicket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return Objects.equals(id, comment.id) &&
                Objects.equals(commentator, comment.commentator) &&
                Objects.equals(text, comment.text) &&
                Objects.equals(date, comment.date) &&
                Objects.equals(commentedTicket, comment.commentedTicket);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, commentator, text, date, commentedTicket);
    }
}
