package epam.training.helpdesk.domain;

public enum Urgency {
    LOW,
    AVERAGE,
    HIGH,
    CRITICAL
}
