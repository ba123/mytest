package epam.training.helpdesk.domain;

public enum Action {
    ACTION_TICKET_CREATED("Ticket is created"),
    ACTION_TICKET_EDITED("Ticket is edited"),
    ACTION_TICKET_CHANGED("Ticket status is changed"),
    ACTION_FILE_ATTACHED("File is attached"),
    ACTION_FILE_REMOVED("File is removed");

    private final String actionText;

    private Action(String actionText) {
        this.actionText = actionText;
    }

    public String getActionText() {
        return actionText;
    }
}
