package epam.training.helpdesk.domain;

public enum State {
    DRAFT,
    NEW,
    APPROVED,
    DECLINED,
    IN_PROGRESS,
    DONE,
    CANCELLED
}
