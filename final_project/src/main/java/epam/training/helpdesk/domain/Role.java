package epam.training.helpdesk.domain;

public enum Role {
    EMPLOYEE,
    MANAGER,
    ENGINEER;
}
