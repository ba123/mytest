package epam.training.helpdesk.domain;

public enum SortingTrend {
    UNDEFINED,
    ASCENDING,
    DESCENDING
}
