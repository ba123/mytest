package epam.training.helpdesk.domain;

public class AuthenticationResult {
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public AuthenticationResult(Role role) {
        this.role = role;
    }
}
