package epam.training.helpdesk.domain;

/**
 * Class for storing allowed actions
 */
public class Permission {
    private Boolean submitAllowed;
    private Boolean cancelAllowed;
    private Boolean approveAllowed;
    private Boolean declineAllowed;
    private Boolean assignToMeAllowed;
    private Boolean doneAllowed;
    private Boolean feedbackViewingAllowed;
    private Boolean feedbackAdditionAllowed;

    public Permission(boolean submitAllowed, boolean cancelAllowed, boolean approveAllowed, boolean declineAllowed, boolean assignToMeAllowed, boolean doneAllowed, boolean feedbackViewingAllowed, boolean feedbackAdditionAllowed) {
        this.submitAllowed = submitAllowed;
        this.cancelAllowed = cancelAllowed;
        this.approveAllowed = approveAllowed;
        this.declineAllowed = declineAllowed;
        this.assignToMeAllowed = assignToMeAllowed;
        this.doneAllowed = doneAllowed;
        this.feedbackViewingAllowed = feedbackViewingAllowed;
        this.feedbackAdditionAllowed = feedbackAdditionAllowed;
    }

    public Boolean getSubmitAllowed() {
        return submitAllowed;
    }

    public void setSubmitAllowed(Boolean submitAllowed) {
        this.submitAllowed = submitAllowed;
    }

    public Boolean getCancelAllowed() {
        return cancelAllowed;
    }

    public void setCancelAllowed(Boolean cancelAllowed) {
        this.cancelAllowed = cancelAllowed;
    }

    public Boolean getApproveAllowed() {
        return approveAllowed;
    }

    public void setApproveAllowed(Boolean approveAllowed) {
        this.approveAllowed = approveAllowed;
    }

    public Boolean getDeclineAllowed() {
        return declineAllowed;
    }

    public void setDeclineAllowed(Boolean declineAllowed) {
        this.declineAllowed = declineAllowed;
    }

    public Boolean getAssignToMeAllowed() {
        return assignToMeAllowed;
    }

    public void setAssignToMeAllowed(Boolean assignToMeAllowed) {
        this.assignToMeAllowed = assignToMeAllowed;
    }

    public Boolean getDoneAllowed() {
        return doneAllowed;
    }

    public void setDoneAllowed(Boolean doneAllowed) {
        this.doneAllowed = doneAllowed;
    }

    public Boolean getFeedbackViewingAllowed() {
        return feedbackViewingAllowed;
    }

    public void setFeedbackViewingAllowed(Boolean feedbackViewingAllowed) {
        this.feedbackViewingAllowed = feedbackViewingAllowed;
    }

    public Boolean getFeedbackAdditionAllowed() {
        return feedbackAdditionAllowed;
    }

    public void setFeedbackAdditionAllowed(Boolean feedbackAdditionAllowed) {
        this.feedbackAdditionAllowed = feedbackAdditionAllowed;
    }
}
