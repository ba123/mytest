package epam.training.helpdesk.domain;

public enum SortingField {
    SORT_DEFAULT(""),
    SORT_BY_ID("id"),
    SORT_BY_NAME("name"),
    SORT_BY_DESIRED_DATE("desiredResolutionDate"),
    SORT_BY_URGENCY("urgency"),
    SORT_BY_STATUS("state");
    private final String fieldName;

    private SortingField(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return this.fieldName;
    }
}
