package epam.training.helpdesk.validator.controller.sort;

import epam.training.helpdesk.domain.SortingOrder;

public interface SortOrderValidator {
    SortingOrder validate(String order);
}
