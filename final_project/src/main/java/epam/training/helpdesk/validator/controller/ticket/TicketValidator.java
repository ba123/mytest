package epam.training.helpdesk.validator.controller.ticket;

import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.dto.ticket.TicketDto;

public interface TicketValidator {
    void validateDto(TicketDto ticketDto);
    void validate(Ticket ticket);
}
