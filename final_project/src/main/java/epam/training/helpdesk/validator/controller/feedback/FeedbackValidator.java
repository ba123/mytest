package epam.training.helpdesk.validator.controller.feedback;

import epam.training.helpdesk.dto.feedback.FeedbackDto;

public interface FeedbackValidator {
    void validate(FeedbackDto dto);
}
