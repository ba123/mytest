package epam.training.helpdesk.validator.controller.feedback.impl;

import epam.training.helpdesk.dto.feedback.FeedbackDto;
import epam.training.helpdesk.exception.validation.ValidationException;
import epam.training.helpdesk.validator.controller.feedback.FeedbackValidator;
import org.springframework.stereotype.Component;

@Component
public class FeedbackValidatorImpl implements FeedbackValidator {
    private static final Integer RATE_MIN_VALUE = 1;
    private static final Integer RATE_MAX_VALUE = 5;
    private static final String VALIDATION_FAILED_MESSAGE = "Feedback validation failed";
    private static final Integer MAX_TEXT_LENGTH = 500;

    private void validateRate(Integer rate) {
        if (rate == null || rate < RATE_MIN_VALUE || rate > RATE_MAX_VALUE) {
            throw new ValidationException(VALIDATION_FAILED_MESSAGE);
        }
    }

    private void validateText(String text) {
        if (text != null && text.length() > MAX_TEXT_LENGTH) {
            throw new ValidationException(VALIDATION_FAILED_MESSAGE);
        }
    }

    @Override
    public void validate(FeedbackDto dto) {
        if (dto == null) {
            throw new ValidationException(VALIDATION_FAILED_MESSAGE);
        }
        validateRate(dto.getRate());
        validateText(dto.getText());
    }
}
