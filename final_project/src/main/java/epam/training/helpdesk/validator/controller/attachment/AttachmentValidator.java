package epam.training.helpdesk.validator.controller.attachment;

import epam.training.helpdesk.dto.attachment.AttachmentDto;

public interface AttachmentValidator {
    void validateNew(AttachmentDto attachmentDto);
}
