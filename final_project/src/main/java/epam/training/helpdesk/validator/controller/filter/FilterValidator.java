package epam.training.helpdesk.validator.controller.filter;

public interface FilterValidator {
    void validate(String filter);
}
