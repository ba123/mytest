package epam.training.helpdesk.validator.controller.comment.impl;

import epam.training.helpdesk.dto.comment.CommentDto;
import epam.training.helpdesk.exception.validation.MalformedDataException;
import epam.training.helpdesk.exception.validation.ValidationException;
import epam.training.helpdesk.service.TicketService;
import epam.training.helpdesk.validator.controller.comment.CommentValidator;
import org.springframework.stereotype.Component;

import static java.util.regex.Pattern.matches;

@Component
public class CommentValidatorImpl implements CommentValidator {

    private static final String COMMENT_VALIDATION_REGEXP = "^[~\\.\"(),:;<>@\\[\\]!#$%&`\\*\\+\\-/=\\?^_'{}|a-zA-Z\\d ]*$";
    private static final String COMMENT_VALIDATION_FAILED_MESSAGE = "Comment validation failed";
    private static final Integer COMMENT_LENGTH_MAX = 500;
    private static final Integer COMMENT_LENGTH_MIN = 1;

    private TicketService ticketService;

    public CommentValidatorImpl(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    private void validateTextLength(CommentDto dto) {
        if ((dto.getText().length() < COMMENT_LENGTH_MIN) || dto.getText().length() > COMMENT_LENGTH_MAX) {
            throw new ValidationException(COMMENT_VALIDATION_FAILED_MESSAGE);
        }
    }

    private void validateTextChars(String text) {
        if (!matches(COMMENT_VALIDATION_REGEXP, text)) {
            throw new ValidationException(COMMENT_VALIDATION_FAILED_MESSAGE);
        }
    }

    private void validateTicket(Integer ticketId) {
        ticketService.getTicket(ticketId).orElseThrow(() -> new ValidationException(COMMENT_VALIDATION_FAILED_MESSAGE));
    }

    @Override
    public void validateDto(CommentDto dto) {
        if (dto == null || dto.getText() == null || dto.getTicketId() == null || dto.getUserId() == null) {
            throw new MalformedDataException(COMMENT_VALIDATION_FAILED_MESSAGE);
        }
        validateTextLength(dto);
        validateTextChars(dto.getText());
        validateTicket(dto.getTicketId());
    }
}
