package epam.training.helpdesk.validator.controller.attachment.impl;

import epam.training.helpdesk.dto.attachment.AttachmentDto;
import epam.training.helpdesk.exception.validation.ValidationException;
import epam.training.helpdesk.validator.controller.attachment.AttachmentValidator;


public class AttachmentValidatorImpl implements AttachmentValidator {
    private static final Integer MAX_ATTACHMENT_SIZE = 5 * 1024 * 1024;

    private boolean validateSize(AttachmentDto dto) {
        return (dto.getFileBody().length <= MAX_ATTACHMENT_SIZE);
    }

    @Override
    public void validateNew(AttachmentDto attachmentDto) {
        if (!validateSize(attachmentDto)) {
            throw new ValidationException("Bad attachment");
        }

    }
}
