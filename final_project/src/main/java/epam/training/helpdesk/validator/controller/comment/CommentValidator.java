package epam.training.helpdesk.validator.controller.comment;

import epam.training.helpdesk.dto.comment.CommentDto;

public interface CommentValidator {
    void validateDto(CommentDto dto);
}
