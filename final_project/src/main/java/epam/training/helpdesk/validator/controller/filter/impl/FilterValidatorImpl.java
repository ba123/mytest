package epam.training.helpdesk.validator.controller.filter.impl;

import epam.training.helpdesk.exception.validation.ValidationException;
import epam.training.helpdesk.validator.controller.filter.FilterValidator;
import org.springframework.stereotype.Component;

import static java.util.regex.Pattern.matches;

@Component
public class FilterValidatorImpl implements FilterValidator {
    private static final String FILTER_VALIDATION_REGEXP = "^[~\\.\"(),:;<>@\\[\\]!#$%&`\\*\\+\\-/=\\?^_'{}|a-zA-Z\\d ]*$";
    private static final String FILTER_VALIDATION_FAILED_MESSAGE = "Filter validation failed";
    @Override
    public void validate(String filter){
        if (!matches(FILTER_VALIDATION_REGEXP, filter)) {
            throw new ValidationException(FILTER_VALIDATION_FAILED_MESSAGE);
        }
    }
}
