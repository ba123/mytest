package epam.training.helpdesk.validator.controller.sort.impl;

import epam.training.helpdesk.domain.SortingField;
import epam.training.helpdesk.domain.SortingOrder;
import epam.training.helpdesk.domain.SortingTrend;
import epam.training.helpdesk.exception.validation.MalformedDataException;
import epam.training.helpdesk.validator.controller.sort.SortOrderValidator;


public class SortOrderValidatorImpl implements SortOrderValidator {
    private static final String ORDER_DEFAULT_STRING = "default";
    private static final String VALIDATION_FAILED_EXCEPTION_MESSAGE = "Failed validation of the sorting order";

    @Override
    public SortingOrder validate(String order) {
        order = order.toLowerCase();
        SortingOrder resultOrder = new SortingOrder(SortingField.SORT_DEFAULT, SortingTrend.UNDEFINED);
        if (order.equalsIgnoreCase(ORDER_DEFAULT_STRING)) {
            resultOrder.setField(SortingField.SORT_DEFAULT);
            resultOrder.setTrend(SortingTrend.UNDEFINED);
        } else {
            if (order.length() < 2) {
                throw new MalformedDataException(VALIDATION_FAILED_EXCEPTION_MESSAGE);
            }
            char sign = order.charAt(0);
            if (sign == '-') {
                resultOrder.setTrend(SortingTrend.DESCENDING);
            } else if (sign == '+') {
                resultOrder.setTrend(SortingTrend.ASCENDING);
            } else {
                throw new MalformedDataException(VALIDATION_FAILED_EXCEPTION_MESSAGE);
            }
            String clearOrder = order.substring(1);
            switch (clearOrder) {
                case "byid": {
                    resultOrder.setField(SortingField.SORT_BY_ID);
                    break;
                }
                case "byname": {
                    resultOrder.setField(SortingField.SORT_BY_NAME);
                    break;
                }
                case "bydesireddate": {
                    resultOrder.setField(SortingField.SORT_BY_DESIRED_DATE);
                    break;
                }
                case "byurgency": {
                    resultOrder.setField(SortingField.SORT_BY_URGENCY);
                    break;
                }
                case "bystatus": {
                    resultOrder.setField(SortingField.SORT_BY_STATUS);
                    break;
                }
                default: {
                    throw new MalformedDataException(VALIDATION_FAILED_EXCEPTION_MESSAGE);
                }
            }
        }
        return resultOrder;
    }
}
