package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.History;

import java.util.List;
import java.util.Optional;

public interface HistoryRepository {
    List<History> getTicketHistory(Integer ticketId, Integer limit);

    Optional<History> addHistory(History history);
}
