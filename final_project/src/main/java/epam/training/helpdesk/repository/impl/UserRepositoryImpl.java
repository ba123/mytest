package epam.training.helpdesk.repository.impl;

import epam.training.helpdesk.domain.Role;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.repository.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private static final String QUERY_FIND_USER_BY_LOGIN = "from User where email = :login";
    private static final String QUERY_FIND_USERS_BY_ROLE = "from User where role=:role";

    /**
     * Session factory for Hibernate to use.
     */
    private SessionFactory sessionFactory;

    public UserRepositoryImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<User> getUserByLogin(final String login) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_FIND_USER_BY_LOGIN);
        query.setParameter("login", login);
        User user = (User) query.uniqueResult();
        return Optional.ofNullable(user);
    }

    @Override
    public Optional<User> getUserById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> q = cb.createQuery(User.class);
        Root<User> root = q.from(User.class);
        q.select(root);
        q.where(cb.equal(root.get("id"), id));
        User user = session.createQuery(q).getSingleResult();
        return Optional.ofNullable(user);
    }

    private List<User> getUsersByRole(Role role) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_FIND_USERS_BY_ROLE);
        query.setParameter("role", role);
        return query.getResultList();
    }

    @Override
    public List<User> getManagers() {
        return getUsersByRole(Role.MANAGER);
    }

    @Override
    public List<User> getEngineers() {
        return getUsersByRole(Role.ENGINEER);
    }

}
