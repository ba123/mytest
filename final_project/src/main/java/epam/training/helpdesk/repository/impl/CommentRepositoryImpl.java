package epam.training.helpdesk.repository.impl;

import epam.training.helpdesk.domain.Comment;
import epam.training.helpdesk.repository.CommentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class CommentRepositoryImpl implements CommentRepository {
    /**
     * Session factory for Hibernate to use.
     */
    private final SessionFactory sessionFactory;

    /**
     * Constructor.
     *
     * @param sessionFactory Session factory to use.
     */
    public CommentRepositoryImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Comment> addComment(Comment comment) {
        Session session = sessionFactory.getCurrentSession();
        session.save(comment);
        return Optional.ofNullable(comment);
    }

    @Override
    public List<Comment> getTicketComments(Integer ticketId, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Comment> q = cb.createQuery(Comment.class);
        Root<Comment> root = q.from(Comment.class);
        q.select(root);
        q.orderBy(cb.desc(root.get("date")));
        q.where(cb.equal(root.get("commentedTicket"), ticketId));
        if (limit != 0) {
            return session.createQuery(q).setMaxResults(limit).getResultList();
        } else {
            return session.createQuery(q).getResultList();
        }
    }
}
