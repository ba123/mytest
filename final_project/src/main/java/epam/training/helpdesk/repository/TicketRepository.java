package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.Permission;
import epam.training.helpdesk.domain.SortingOrder;
import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface TicketRepository {
    List<Ticket> getAllTickets();

    List<Ticket> getTicketsByUser(User user, SortingOrder sortingOrder, boolean personalTicketsOnly, String filter);

    List<Integer> getAllowedTicketsId(User user);

    Optional<Ticket> addTicket(Ticket ticket);

    Optional<Ticket> updateTicket(Ticket ticket);

    boolean isTicketVisible(Integer ticketId, User user);

    Optional<Ticket> getTicketChecked(Integer ticketId, User user);

    Optional<Ticket> getTicket(Integer id);

    Permission getAllowedActions(Integer ticketId, User user);

    void changeTicketStatus(Integer ticketId, State state);

    void updateApprover(Integer ticketId, User approver);

    void updateAssignee(Integer ticketId, User assignee);

    List<Integer> getTicketAttachmentsIds (Integer ticketId);

}
