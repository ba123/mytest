package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.Attachment;

import java.util.Optional;

public interface AttachmentRepository {
    Optional<Attachment> getAttachment(Integer attachmentId);

    Optional<Attachment> addAttachment(Attachment attachment);

    Optional<Attachment> updateAttachment(Attachment attachment);

    void deleteAttachment(Integer attachmentId);
}
