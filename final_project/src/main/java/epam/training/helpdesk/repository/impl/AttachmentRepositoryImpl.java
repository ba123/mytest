package epam.training.helpdesk.repository.impl;

import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.repository.AttachmentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

@Repository
public class AttachmentRepositoryImpl implements AttachmentRepository {

    /**
     * Session factory for Hibernate to use.
     */
    private final SessionFactory sessionFactory;

    public AttachmentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Attachment> getAttachment(final Integer id) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Attachment> q = cb.createQuery(Attachment.class);
        Root<Attachment> root = q.from(Attachment.class);
        q.select(root);
        q.where(cb.equal(root.get("id"), id));
        Attachment attachment = session.createQuery(q).getSingleResult();
        return Optional.ofNullable(attachment);
    }

    @Override
    public Optional<Attachment> addAttachment(Attachment attachment) {
        Session session = sessionFactory.getCurrentSession();
        session.save(attachment);
        return Optional.ofNullable(attachment);
    }

    @Override
    public Optional<Attachment> updateAttachment(Attachment attachment) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(attachment);
        return Optional.ofNullable(attachment);
    }

    @Override
    public void deleteAttachment(Integer attachmentId) {
        Session session = sessionFactory.getCurrentSession();
        Attachment attachment = session.find(Attachment.class, attachmentId);
        if (attachment != null) {
            session.delete(attachment);
            session.flush();
        }
    }
}
