package epam.training.helpdesk.repository.impl;

import epam.training.helpdesk.domain.Permission;
import epam.training.helpdesk.domain.SortingField;
import epam.training.helpdesk.domain.SortingOrder;
import epam.training.helpdesk.domain.SortingTrend;
import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.repository.TicketRepository;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static epam.training.helpdesk.domain.Role.EMPLOYEE;
import static epam.training.helpdesk.domain.Role.MANAGER;

@Repository
public class TicketRepositoryImpl implements TicketRepository {

    private static final String FIELD_NAME_ROLE = "role";
    private static final String FIELD_NAME_STATE = "state";
    private static final String FIELD_NAME_OWNER = "owner";
    private static final String FIELD_NAME_ASSIGNEE = "assignee";
    private static final String FIELD_NAME_ID = "id";
    private static final String QUERY_PARAMETER_NAME_TICKET_ID = "ticketId";
    private static final String QUERY_PARAMETER_NAME_USER_ID = "userId";
    private static final String UNKNOWN_ROLE_MESSAGE = "Unknown role";
    private static final String TICKET_NOT_FOUND_MESSAGE = "Ticket not found";

    private static final String QUERY_SELECT_OPTION_TICKETS_ID = "select t.id from Ticket t where ";

    private static final String QUERY_SELECT_OPTION_TICKETS = "select t from Ticket t where ";

    private static final String QUERY_CLAUSE_OWN_TICKETS = "t.owner.id = :userId";

    private static final String QUERY_CLAUSE_EMPLOYEE_ALLOWED_TICKETS = QUERY_CLAUSE_OWN_TICKETS;

    private static final String QUERY_GET_EMPLOYEE_ALLOWED_TICKETS_ID =
            QUERY_SELECT_OPTION_TICKETS_ID + QUERY_CLAUSE_EMPLOYEE_ALLOWED_TICKETS;

    private static final String QUERY_GET_EMPLOYEE_ALLOWED_TICKETS =
            QUERY_SELECT_OPTION_TICKETS + QUERY_CLAUSE_EMPLOYEE_ALLOWED_TICKETS;


    private static final String QUERY_CLAUSE_ENGINEER_ALLOWED_TICKETS =
            "(t.owner.role IN ('EMPLOYEE','MANAGER') and t.state = 'APPROVED')"
                    + " or ((t.assignee.id = :userId) and (t.state in ('IN_PROGRESS','DONE')))";
    private static final String QUERY_CLAUSE_ENGINEER_MY_TICKETS_FILTER = " and (t.assignee.id = :userId)";

    private static final String QUERY_GET_ENGINEER_ALLOWED_TICKETS = QUERY_SELECT_OPTION_TICKETS + QUERY_CLAUSE_ENGINEER_ALLOWED_TICKETS;
    private static final String QUERY_GET_ENGINEER_ALLOWED_TICKETS_ID = QUERY_SELECT_OPTION_TICKETS_ID + QUERY_CLAUSE_ENGINEER_ALLOWED_TICKETS;


    private static final String QUERY_CLAUSE_MANAGER_ALLOWED_TICKETS =
            "(t.owner.id = :userId) or ( (t.owner.role = 'EMPLOYEE') and (t.state = 'NEW'))" +
                    " or ( (t.approver.id = :userId) and (t.state in ('APPROVED', 'DECLINED','CANCELLED', 'IN_PROGRESS', 'DONE')))";

    private static final String QUERY_CLAUSE_MANAGER_MY_TICKETS_FILTER = " and ((t.owner.id = :userId) or ((t.approver.id = :userId) and (t.state in ('APPROVED'))))";
    private static final String QUERY_GET_MANAGER_ALLOWED_TICKETS = QUERY_SELECT_OPTION_TICKETS + QUERY_CLAUSE_MANAGER_ALLOWED_TICKETS;
    private static final String QUERY_GET_MANAGER_ALLOWED_TICKETS_ID = QUERY_SELECT_OPTION_TICKETS_ID + QUERY_CLAUSE_MANAGER_ALLOWED_TICKETS;


    private static final String QUERY_CLAUSE_FILTER_TICKET_ID = " and (t.id = :ticketId)";

    private static final String URGENCY_SORT =
            "(CASE WHEN t.urgency = 'LOW' THEN 1" +
                    " WHEN t.urgency = 'AVERAGE' THEN '2'" +
                    " WHEN t.urgency = 'HIGH' THEN '3'" +
                    " WHEN t.urgency = 'CRITICAL' THEN '4'" +
                    " ELSE t.urgency END)";
    private static final String QUERY_DEFAULT_SORTING_ORDER = " order by " + URGENCY_SORT + " DESC, t.desiredResolutionDate DESC";


    private static final String QUERY_GET_EMPLOYEE_AND_MANAGER_ADD_FEEDBACK_PERMISSION =
            "select t.id from Ticket t LEFT JOIN Feedback f on t.id = f.feedbackedTicket " +
                    "where f.feedbackedTicket is null and t.owner.id = :userId and t.state=:state and t.id = :ticketId";
    private static final String QUERY_GET_PERMISSION_EMPLOYEE_VIEW_FEEDBACK =
            "select t.id from Ticket t INNER JOIN Feedback f on t.id = f.feedbackedTicket " +
                    "where t.owner.id = :userId and t.id = :ticketId";
    private static final String QUERY_GET_PERMISSION_MANAGER_VIEW_FEEDBACK =
            "select t.id from Ticket t INNER JOIN Feedback f on t.id = f.feedbackedTicket " +
                    "where t.owner.id = :userId or t.owner.role=:role and t.id = :ticketId";


    private static final String QUERY_CLAUSE_TICKETS_FILTERING =
            " and (( CAST(t.id  as string) like :filter) or ( t.name like :filter) or " +
                    "(FORMATDATETIME(t.desiredResolutionDate, 'dd/MM/yyyy') like :filter) or (CAST(t.urgency as string) like :filter) or (t.state like :filter))";

    /**
     * Session factory for Hibernate to use.
     */
    private final SessionFactory sessionFactory;

    /**
     * Constructor.
     *
     * @param sessionFactory Session factory to use.
     */
    public TicketRepositoryImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Return all goods.
     *
     * @return List of the tickets.
     */
    @Override
    public List<Ticket> getAllTickets() {
        Session session = sessionFactory.getCurrentSession();
        TypedQuery<Ticket> query = session.createQuery("from Ticket", Ticket.class);
        List<Ticket> tickets = query.getResultList();
        for (Ticket ticket : tickets) {
            Hibernate.initialize(ticket.getAssignee());
            Hibernate.initialize(ticket.getOwner());
            Hibernate.initialize(ticket.getApprover());
        }
        return tickets;
    }

    private String sortingOrderToSqlQuery(SortingOrder sortingOrder) {
        String order;
        if (sortingOrder.getField() == SortingField.SORT_DEFAULT) {
            order = QUERY_DEFAULT_SORTING_ORDER;
        } else {
            if (sortingOrder.getField() == SortingField.SORT_BY_URGENCY) {
                order = " ORDER BY " + URGENCY_SORT;
            } else {
                order = " ORDER BY " + sortingOrder.getField().getFieldName();
            }
            if (sortingOrder.getTrend() == SortingTrend.DESCENDING) {
                order = order + " DESC";
            }
        }
        return order;
    }

    @Override
    public List<Ticket> getTicketsByUser(User user, SortingOrder sortingOrder, boolean personalTicketsOnly, String filter) {
        String hql;
        switch (user.getRole()) {
            case EMPLOYEE:
                hql = QUERY_GET_EMPLOYEE_ALLOWED_TICKETS;
                break;
            case ENGINEER:
                hql = QUERY_GET_ENGINEER_ALLOWED_TICKETS;
                if (personalTicketsOnly) {
                    hql = hql + QUERY_CLAUSE_ENGINEER_MY_TICKETS_FILTER;
                }
                break;
            case MANAGER:
                hql = QUERY_GET_MANAGER_ALLOWED_TICKETS;
                if (personalTicketsOnly) {
                    hql = hql + QUERY_CLAUSE_MANAGER_MY_TICKETS_FILTER;
                }
                break;
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
        if (!filter.isEmpty()) {
            hql = hql + QUERY_CLAUSE_TICKETS_FILTERING;
        }
        hql = hql + sortingOrderToSqlQuery(sortingOrder);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql, Ticket.class);
        if (!filter.isEmpty()) {
            query.setParameter("filter", "%" + filter + "%");
        }
        query.setParameter(QUERY_PARAMETER_NAME_USER_ID, user.getId());
        return query.getResultList();
    }


    @Override
    public boolean isTicketVisible(Integer ticketId, User user) {
        String hql;
        switch (user.getRole()) {
            case EMPLOYEE:
                hql = QUERY_GET_EMPLOYEE_ALLOWED_TICKETS_ID + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            case MANAGER:
                hql = QUERY_GET_MANAGER_ALLOWED_TICKETS_ID + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            case ENGINEER:
                hql = QUERY_GET_ENGINEER_ALLOWED_TICKETS_ID + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql, Integer.class);
        query.setParameter(QUERY_PARAMETER_NAME_USER_ID, user.getId());
        query.setParameter(QUERY_PARAMETER_NAME_TICKET_ID, ticketId);
        boolean found = true;
        try {
            query.getSingleResult();
        } catch (NoResultException e) {
            found = false;
        }
        return found;
    }

    @Override
    public Optional<Ticket> getTicketChecked(Integer ticketId, User user) {
        String hql;
        switch (user.getRole()) {
            case EMPLOYEE:
                hql = QUERY_GET_EMPLOYEE_ALLOWED_TICKETS + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            case MANAGER:
                hql = QUERY_GET_MANAGER_ALLOWED_TICKETS + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            case ENGINEER:
                hql = QUERY_GET_ENGINEER_ALLOWED_TICKETS + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql, Ticket.class);
        query.setParameter(QUERY_PARAMETER_NAME_USER_ID, user.getId());
        query.setParameter(QUERY_PARAMETER_NAME_TICKET_ID, ticketId);
        Ticket ticket;
        try {
            ticket = (Ticket) query.getSingleResult();
        } catch (NoResultException e) {
            ticket = null;
        }
        return Optional.ofNullable(ticket);
    }

    @Override
    public List<Integer> getAllowedTicketsId(User user) {
        String hql;
        switch (user.getRole()) {
            case EMPLOYEE:
                hql = QUERY_GET_EMPLOYEE_ALLOWED_TICKETS_ID;
                break;
            case MANAGER:
                hql = QUERY_GET_MANAGER_ALLOWED_TICKETS_ID;
                break;
            case ENGINEER:
                hql = QUERY_GET_ENGINEER_ALLOWED_TICKETS_ID;
                break;
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql, Integer.class);
        query.setParameter(QUERY_PARAMETER_NAME_USER_ID, user.getId());
        return  query.getResultList();
    }

    @Override
    public Optional<Ticket> addTicket(Ticket ticket) {
        Session session = sessionFactory.getCurrentSession();
        session.save(ticket);
        session.flush();
        return Optional.ofNullable(ticket);
    }

    @Override
    public Optional<Ticket> updateTicket(Ticket ticket) {
        Session session = sessionFactory.getCurrentSession();
        session.update(ticket);
        return Optional.ofNullable(ticket);
    }

    @Override
    public Optional<Ticket> getTicket(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Ticket> q = cb.createQuery(Ticket.class);
        Root<Ticket> root = q.from(Ticket.class);
        q.select(root);
        q.where(cb.equal(root.get("id"), id));
        Ticket ticket = null;
        try {
            ticket = session.createQuery(q).getSingleResult();
        } catch (NoResultException e) {
            throw new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE);
        }
        Hibernate.initialize(ticket.getHistoryRecords());
        return Optional.ofNullable(ticket);
    }

    @Override
    public List <Integer> getTicketAttachmentsIds (Integer ticketId){
        String hql = "select att.id from Attachment att where att.attachmentTicket = :ticketId";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql, Integer.class);
        query.setParameter(QUERY_PARAMETER_NAME_TICKET_ID, ticketId);
        return  query.getResultList();
    }

    private boolean getEmployeeOrManagerAddFeedbackPermission(Integer ticketId, User user) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_EMPLOYEE_AND_MANAGER_ADD_FEEDBACK_PERMISSION, Integer.class);
        query.setParameter(QUERY_PARAMETER_NAME_TICKET_ID, ticketId);
        query.setParameter(QUERY_PARAMETER_NAME_USER_ID, user.getId());
        query.setParameter(FIELD_NAME_STATE, State.DONE);
        try {
            query.getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    private boolean getEmployeeAddFeedbackPermission(Integer ticketId, User user) {
        return getEmployeeOrManagerAddFeedbackPermission(ticketId, user);
    }

    private boolean getManagerAddFeedbackPermission(Integer ticketId, User user) {
        return getEmployeeOrManagerAddFeedbackPermission(ticketId, user);
    }

    private boolean getEmployeeViewFeedbackPermission(Integer ticketId, User user) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_PERMISSION_EMPLOYEE_VIEW_FEEDBACK, Integer.class);
        query.setParameter(QUERY_PARAMETER_NAME_TICKET_ID, ticketId);
        query.setParameter(QUERY_PARAMETER_NAME_USER_ID, user.getId());
        try {
            query.getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    private boolean getManagerViewFeedbackPermission(Integer ticketId, User user) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_PERMISSION_MANAGER_VIEW_FEEDBACK, Integer.class);
        query.setParameter(FIELD_NAME_ROLE, EMPLOYEE);
        query.setParameter(QUERY_PARAMETER_NAME_USER_ID, user.getId());
        query.setParameter(QUERY_PARAMETER_NAME_TICKET_ID, ticketId);
        try {
            query.getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    private boolean getEmployeeSubmitOrCancelPermission(Integer ticketId, User user) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Ticket> q = cb.createQuery(Ticket.class);
        Root<Ticket> root = q.from(Ticket.class);
        q.select(root);
        Predicate prRequestedTicket = cb.equal(root.get(FIELD_NAME_ID), ticketId);
        Predicate prOwnTicket = cb.equal(root.get(FIELD_NAME_OWNER), user.getId());
        Predicate prDraftState = cb.equal(root.get(FIELD_NAME_STATE), State.DRAFT);
        Predicate prDeclinedState = cb.equal(root.get(FIELD_NAME_STATE), State.DECLINED);

        q.where(cb.and(prRequestedTicket,
                cb.and(prOwnTicket,
                        cb.or(prDraftState, prDeclinedState))));
        try {
            session.createQuery(q).getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    private boolean getEmployeeSubmitPermission(Integer ticketId, User user) {
        return getEmployeeSubmitOrCancelPermission(ticketId, user);
    }

    private boolean getEmployeeCancelPermission(Integer ticketId, User user) {
        return getEmployeeSubmitOrCancelPermission(ticketId, user);
    }

    private boolean getManagerSubmitPermission(Integer ticketId, User user) {
        return getEmployeeSubmitOrCancelPermission(ticketId, user);
    }

    private boolean getManagerCancelPermission(Integer ticketId, User user) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Ticket> q = cb.createQuery(Ticket.class);
        Root<Ticket> root = q.from(Ticket.class);
        q.select(root);
        Predicate prRequestedTicket = cb.equal(root.get(FIELD_NAME_ID), ticketId);
        Predicate prOwnTicket = cb.equal(root.get(FIELD_NAME_OWNER), user.getId());
        Predicate prDraftState = cb.equal(root.get(FIELD_NAME_STATE), State.DRAFT);
        Predicate prDeclinedState = cb.equal(root.get(FIELD_NAME_STATE), State.DECLINED);
        Predicate prEmployeesTicket = cb.equal(root.get(FIELD_NAME_OWNER).get(FIELD_NAME_ROLE), EMPLOYEE);
        Predicate prNewState = cb.equal(root.get(FIELD_NAME_STATE), State.NEW);
        Predicate prNewEmployees = cb.and(prEmployeesTicket, prNewState);
        Predicate prOwnDraftOrNew = cb.and(prOwnTicket, cb.or(prDraftState, prDeclinedState));

        q.where(cb.and(prRequestedTicket,
                cb.or(prNewEmployees, prOwnDraftOrNew)));
        try {
            session.createQuery(q).getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    private boolean getManagerApproveOrDeclinePermission(Integer ticketId, User user) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Ticket> q = cb.createQuery(Ticket.class);
        Root<Ticket> root = q.from(Ticket.class);
        q.select(root);
        Predicate prRequestedTicket = cb.equal(root.get(FIELD_NAME_ID), ticketId);
        Predicate prOwnTicket = cb.equal(root.get(FIELD_NAME_OWNER), user.getId());
        Predicate prNewState = cb.equal(root.get(FIELD_NAME_STATE), State.NEW);
        Predicate prEmployeesTicket = cb.equal(root.get(FIELD_NAME_OWNER).get(FIELD_NAME_ROLE), EMPLOYEE);

        Predicate prOwnOrEmployees = cb.or(prOwnTicket, prEmployeesTicket);
        q.where(cb.and(prRequestedTicket,
                cb.and(prOwnOrEmployees, prNewState)));
        try {
            session.createQuery(q).getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    private boolean getManagerApprovePermission(Integer ticketId, User user) {
        return getManagerApproveOrDeclinePermission(ticketId, user);
    }

    private boolean getManagerDeclinePermission(Integer ticketId, User user) {
        return getManagerApproveOrDeclinePermission(ticketId, user);
    }

    private boolean getEngineerAssignToMeOrCancelPermission(Integer ticketId) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Ticket> q = cb.createQuery(Ticket.class);
        Root<Ticket> root = q.from(Ticket.class);
        q.select(root);
        Predicate prRequestedTicket = cb.equal(root.get(FIELD_NAME_ID), ticketId);
        Predicate prManagersTicket = cb.equal(root.get(FIELD_NAME_OWNER).get(FIELD_NAME_ROLE), MANAGER);
        Predicate prEmployeeTicket = cb.equal(root.get(FIELD_NAME_OWNER).get(FIELD_NAME_ROLE), EMPLOYEE);
        Predicate prApprovedState = cb.equal(root.get(FIELD_NAME_STATE), State.APPROVED);

        Predicate prEmployeesOrManagersTicket = cb.or(prEmployeeTicket, prManagersTicket);
        q.where(cb.and(prRequestedTicket,
                cb.and(prEmployeesOrManagersTicket, prApprovedState)));
        try {
            session.createQuery(q).getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    private boolean getEngineerAssignToMePermission(Integer ticketId) {
        return getEngineerAssignToMeOrCancelPermission(ticketId);
    }

    private boolean getEngineerCancelPermission(Integer ticketId) {
        return getEngineerAssignToMeOrCancelPermission(ticketId);
    }

    private boolean getEngineerDonePermission(Integer ticketId, User user) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Ticket> q = cb.createQuery(Ticket.class);
        Root<Ticket> root = q.from(Ticket.class);
        q.select(root);
        Predicate prRequestedTicket = cb.equal(root.get(FIELD_NAME_ID), ticketId);
        Predicate prAssignedTicket = cb.equal(root.get(FIELD_NAME_ASSIGNEE), user.getId());
        Predicate prInProgressState = cb.equal(root.get(FIELD_NAME_STATE), State.IN_PROGRESS);

        Predicate prAssignedInProgress = cb.and(prAssignedTicket, prInProgressState);
        q.where(cb.and(prRequestedTicket, prAssignedInProgress));
        try {
            session.createQuery(q).getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    private Permission getEmployeePermissions(Integer ticketId, User user) {
        return new Permission(getEmployeeSubmitPermission(ticketId, user),
                getEmployeeCancelPermission(ticketId, user),
                false, false, false, false, getEmployeeViewFeedbackPermission(ticketId, user), getEmployeeAddFeedbackPermission(ticketId, user));
    }

    private Permission getManagerPermissions(Integer ticketId, User user) {
        return new Permission(
                getManagerSubmitPermission(ticketId, user), getManagerCancelPermission(ticketId, user),
                getManagerApprovePermission(ticketId, user), getManagerDeclinePermission(ticketId, user),
                false, false, getManagerViewFeedbackPermission(ticketId, user), getManagerAddFeedbackPermission(ticketId, user));
    }

    private Permission getEngineerPermissions(Integer ticketId, User user) {
        return new Permission(false, getEngineerCancelPermission(ticketId), false, false,
                getEngineerAssignToMePermission(ticketId), getEngineerDonePermission(ticketId, user), false, false);
    }

    @Override
    public Permission getAllowedActions(Integer ticketId, User user) {
        switch (user.getRole()) {
            case EMPLOYEE:
                return getEmployeePermissions(ticketId, user);
            case MANAGER:
                return getManagerPermissions(ticketId, user);
            case ENGINEER:
                return getEngineerPermissions(ticketId, user);
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
    }

    @Override
    public void changeTicketStatus(Integer ticketId, State state) {
        Session session = sessionFactory.getCurrentSession();
        Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        ticket.setState(state);
        session.update(ticket);
        session.flush();
    }

    @Override
    public void updateApprover(Integer ticketId, User approver) {
        Session session = sessionFactory.getCurrentSession();
        Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        ticket.setApprover(approver);
        session.update(ticket);
        session.flush();
    }

    @Override
    public void updateAssignee(Integer ticketId, User assignee) {
        Session session = sessionFactory.getCurrentSession();
        Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        ticket.setAssignee(assignee);
        session.update(ticket);
        session.flush();
    }
}
