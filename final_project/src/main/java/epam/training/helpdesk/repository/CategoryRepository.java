package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository {
    List<Category> getAllCategories();

    Optional<Category> getCategory(Integer id);
}
