package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.Feedback;

import java.util.Optional;

public interface FeedbackRepository {
    Optional<Feedback> addFeedback(Feedback feedback);

    Feedback getFeedback(Integer ticketId);

}
