package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    /**
     * Return User object by login name.
     *
     * @param login login name.
     * @return User object.
     */

    Optional<User> getUserByLogin(String login);

    Optional<User> getUserById(Integer id);

    List<User> getManagers();

    List<User> getEngineers();

}
