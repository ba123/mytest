package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.Comment;

import java.util.List;
import java.util.Optional;

public interface CommentRepository {
    Optional<Comment> addComment(Comment comment);

    List<Comment> getTicketComments(Integer ticketId, Integer limit);
}
