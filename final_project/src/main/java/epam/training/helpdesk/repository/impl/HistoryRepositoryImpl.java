package epam.training.helpdesk.repository.impl;

import epam.training.helpdesk.domain.History;
import epam.training.helpdesk.repository.HistoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
public class HistoryRepositoryImpl implements HistoryRepository {
    /**
     * Session factory for Hibernate to use.
     */
    private final SessionFactory sessionFactory;

    public HistoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<History> getTicketHistory(Integer ticketId, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<History> q = cb.createQuery(History.class);
        Root<History> root = q.from(History.class);
        q.select(root);
        q.orderBy(cb.desc(root.get("date")));
        q.where(cb.equal(root.get("historyTicket"), ticketId));
        if (limit != 0) {
            return session.createQuery(q).setMaxResults(limit).getResultList();
        } else {
            return session.createQuery(q).getResultList();
        }
    }

    @Override
    public Optional<History> addHistory(History history) {
        Session session = sessionFactory.getCurrentSession();
        session.save(history);
        return Optional.ofNullable(history);
    }
}
