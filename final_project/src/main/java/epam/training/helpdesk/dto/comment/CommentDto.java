package epam.training.helpdesk.dto.comment;

import java.sql.Timestamp;

public class CommentDto {
    private Integer id;
    private String text;
    private Timestamp date;
    private Integer userId;
    private Integer ticketId;
    private String username;

    public CommentDto() {
    }

    public CommentDto(Integer id, String text, Timestamp date, Integer userId, Integer ticketId) {
        this.text = text;
        this.date = date;
        this.userId = userId;
        this.ticketId = ticketId;
        this.id = id;
    }

    public CommentDto(Integer id, String text, Timestamp date, String username) {
        this.text = text;
        this.date = date;
        this.username = username;
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }
}
