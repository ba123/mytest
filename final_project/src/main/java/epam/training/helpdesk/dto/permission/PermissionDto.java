package epam.training.helpdesk.dto.permission;

import java.util.Objects;

public class PermissionDto {
    private Integer ticketId;
    private Boolean submitAllowed;
    private Boolean cancelAllowed;
    private Boolean approveAllowed;
    private Boolean declineAllowed;
    private Boolean assignToMeAllowed;
    private Boolean doneAllowed;
    private Boolean feedbackViewingAllowed;
    private Boolean feedbackAddAllowed;

    public PermissionDto() {
    }

    public PermissionDto(Integer ticketId, Boolean submitAllowed, Boolean cancelAllowed,
                         Boolean approveAllowed, Boolean declineAllowed, Boolean assignToMeAllowed,
                         Boolean doneAllowed, Boolean feedbackViewingAllowed, Boolean feedbackAddAllowed) {
        this.submitAllowed = submitAllowed;
        this.cancelAllowed = cancelAllowed;
        this.approveAllowed = approveAllowed;
        this.declineAllowed = declineAllowed;
        this.assignToMeAllowed = assignToMeAllowed;
        this.doneAllowed = doneAllowed;
        this.feedbackViewingAllowed = feedbackViewingAllowed;
        this.feedbackAddAllowed = feedbackAddAllowed;
        this.ticketId = ticketId;
    }

    public Boolean getSubmitAllowed() {
        return submitAllowed;
    }

    public void setSubmitAllowed(Boolean submitAllowed) {
        this.submitAllowed = submitAllowed;
    }

    public Boolean getCancelAllowed() {
        return cancelAllowed;
    }

    public void setCancelAllowed(Boolean cancelAllowed) {
        this.cancelAllowed = cancelAllowed;
    }

    public Boolean getApproveAllowed() {
        return approveAllowed;
    }

    public void setApproveAllowed(Boolean approveAllowed) {
        this.approveAllowed = approveAllowed;
    }

    public Boolean getDeclineAllowed() {
        return declineAllowed;
    }

    public void setDeclineAllowed(Boolean declineAllowed) {
        this.declineAllowed = declineAllowed;
    }

    public Boolean getAssignToMeAllowed() {
        return assignToMeAllowed;
    }

    public void setAssignToMeAllowed(Boolean assignToMeAllowed) {
        this.assignToMeAllowed = assignToMeAllowed;
    }

    public Boolean getDoneAllowed() {
        return doneAllowed;
    }

    public void setDoneAllowed(Boolean doneAllowed) {
        this.doneAllowed = doneAllowed;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public Boolean getFeedbackViewingAllowed() {
        return feedbackViewingAllowed;
    }

    public void setFeedbackViewingAllowed(Boolean feedbackViewingAllowed) {
        this.feedbackViewingAllowed = feedbackViewingAllowed;
    }

    public Boolean getFeedbackAddAllowed() {
        return feedbackAddAllowed;
    }

    public void setFeedbackAddAllowed(Boolean feedbackAddAllowed) {
        this.feedbackAddAllowed = feedbackAddAllowed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PermissionDto that = (PermissionDto) o;
        return Objects.equals(ticketId, that.ticketId) &&
                Objects.equals(submitAllowed, that.submitAllowed) &&
                Objects.equals(cancelAllowed, that.cancelAllowed) &&
                Objects.equals(approveAllowed, that.approveAllowed) &&
                Objects.equals(declineAllowed, that.declineAllowed) &&
                Objects.equals(assignToMeAllowed, that.assignToMeAllowed) &&
                Objects.equals(doneAllowed, that.doneAllowed) &&
                Objects.equals(feedbackViewingAllowed, that.feedbackViewingAllowed) &&
                Objects.equals(feedbackAddAllowed, that.feedbackAddAllowed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ticketId, submitAllowed, cancelAllowed, approveAllowed, declineAllowed, assignToMeAllowed, doneAllowed, feedbackViewingAllowed, feedbackAddAllowed);
    }
}
