package epam.training.helpdesk.dto.ticket;

import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Urgency;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class TicketDto {
    private Integer id;
    private String name;
    private String description;
    private Timestamp createdOn;
    private Date desiredResolutionDate;
    private Integer assigneeId;
    private Integer ownerId;
    private State state;
    private Urgency urgency;
    private Integer categoryId;
    private Set<Integer> attachmentsId;
    private Integer approverId;
    private List<Integer> historyIds;

    public List<Integer> getHistoryIds() {
        return historyIds;
    }

    public void setHistoryIds(List<Integer> historyIds) {
        this.historyIds = historyIds;
    }

    public Integer getApproverId() {
        return approverId;
    }

    public void setApproverId(Integer approverId) {
        this.approverId = approverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Date getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(Date desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public Integer getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(Integer assigneeId) {
        this.assigneeId = assigneeId;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public Set<Integer> getAttachmentsId() {
        return attachmentsId;
    }

    public void setAttachmentsId(Set<Integer> attachmentsId) {
        this.attachmentsId = attachmentsId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}