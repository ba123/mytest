package epam.training.helpdesk.dto.operation;

import java.util.Objects;

public class OperationResultDto {
    private boolean operationComplete;
    private Integer id;

    public boolean isOperationComplete() {
        return operationComplete;
    }

    public void setOperationComplete(boolean operationComplete) {
        this.operationComplete = operationComplete;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public OperationResultDto(boolean operationComplete, Integer id) {
        this.operationComplete = operationComplete;
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OperationResultDto that = (OperationResultDto) o;
        return operationComplete == that.operationComplete &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(operationComplete, id);
    }
}
