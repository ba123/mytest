package epam.training.helpdesk.dto.attachment;

public class AttachmentSimpleDto {
    private Integer id;
    private String filename;

    public AttachmentSimpleDto() {
    }

    public AttachmentSimpleDto(Integer id, String filename) {
        this.id = id;
        this.filename = filename;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
