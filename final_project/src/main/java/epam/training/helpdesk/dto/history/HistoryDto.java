package epam.training.helpdesk.dto.history;

import java.sql.Timestamp;

public class HistoryDto {
    private Integer id;
    private Timestamp date;
    private String action;
    private String username;
    private String description;

    public HistoryDto() {
    }

    public HistoryDto(Integer id, Timestamp date, String action, String username, String description) {
        this.id = id;
        this.date = date;
        this.action = action;
        this.username = username;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        description = description;
    }
}
