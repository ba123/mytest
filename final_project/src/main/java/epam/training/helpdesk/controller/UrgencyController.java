package epam.training.helpdesk.controller;

import epam.training.helpdesk.domain.Urgency;
import epam.training.helpdesk.service.UrgencyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/urgency")
public class UrgencyController {

    private UrgencyService urgencyService;

    public UrgencyController(UrgencyService urgencyService) {
        this.urgencyService = urgencyService;
    }

    @GetMapping(value = "/")
    public ResponseEntity<Urgency[]> getTickets() {
        return ResponseEntity.ok(urgencyService.getUrgencyList());
    }

}
