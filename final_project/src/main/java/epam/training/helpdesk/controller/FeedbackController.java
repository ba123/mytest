package epam.training.helpdesk.controller;

import epam.training.helpdesk.dto.feedback.FeedbackDto;
import epam.training.helpdesk.service.FeedbackService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/feedbacks")
public class FeedbackController {
    private FeedbackService feedbackService;

    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @PutMapping(value = "/{ticketId}")
    public ResponseEntity<Void> addTicket(Authentication authData,
                                                 @RequestBody FeedbackDto feedbackDto,
                                                 @NotBlank(message = "The id cannot be blank")
                                                 @PathVariable final Integer ticketId) {
        feedbackService.addFeedback(feedbackDto,authData, ticketId);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @GetMapping(value = "/{ticketId}")
    public ResponseEntity<FeedbackDto> getTicket(Authentication authData,
                                          @NotBlank(message = "The id cannot be blank")
                                          @PathVariable final Integer ticketId) {
        FeedbackDto dto = feedbackService.getFeedback(authData, ticketId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}
