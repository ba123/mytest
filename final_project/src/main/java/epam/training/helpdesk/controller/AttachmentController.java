package epam.training.helpdesk.controller;

import epam.training.helpdesk.dto.attachment.AttachmentDto;
import epam.training.helpdesk.dto.operation.OperationResultDto;
import epam.training.helpdesk.service.AttachmentService;
import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;

@RestController
@RequestMapping("/attachments")
public class AttachmentController {

    private AttachmentService attachmentService;

    public AttachmentController(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<Resource> getAttachment(
            @NotBlank(message = "The id cannot be blank")
            @PathVariable final Integer id,
            final Authentication authentication) {
        AttachmentDto dto = attachmentService.getAttachmentDto(id, authentication);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.setContentLength(dto.getFileBody().length);
        header.set("Content-Disposition", "attachment; filename=" + dto.getName());
        header.set("Access-Control-Expose-Headers", "Content-Disposition");
        byte[] bytes = ArrayUtils.toPrimitive(dto.getFileBody());
        Resource resource = new ByteArrayResource(bytes);
        return new ResponseEntity<>(resource, header, HttpStatus.OK);
    }

    @PostMapping(value = "/")
    @CrossOrigin
    public ResponseEntity<OperationResultDto> uploadNewFile(@NotNull @RequestParam("file") MultipartFile multipartFile) throws IOException {
        return new ResponseEntity<>(attachmentService.uploadNewFile(multipartFile), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteFile(@NotBlank(message = "The id cannot be blank")
                                           @PathVariable final Integer id) {
        attachmentService.deleteAttachmentChecked(id);
        return ResponseEntity.ok().build();
    }
}