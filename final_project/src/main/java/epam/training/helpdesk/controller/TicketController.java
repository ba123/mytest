package epam.training.helpdesk.controller;

import epam.training.helpdesk.dto.attachment.AttachmentSimpleDto;
import epam.training.helpdesk.dto.history.HistoryDto;
import epam.training.helpdesk.dto.operation.OperationResultDto;
import epam.training.helpdesk.dto.permission.PermissionDto;
import epam.training.helpdesk.dto.ticket.TicketDto;
import epam.training.helpdesk.service.TicketService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/tickets")
public class TicketController {

    private TicketService ticketService;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @GetMapping(value = "/")
    public ResponseEntity<List<TicketDto>> getTickets(final Authentication authentication,
                                                      @RequestParam(required = false, defaultValue = "default") String sort,
                                                      @RequestParam(required = false, defaultValue = "") String filter) {
        return ResponseEntity.ok(ticketService.getTicketsByAuthentication(authentication, sort, false, filter));
    }

    @GetMapping(value = "/{ticketId}")
    public ResponseEntity<TicketDto> getTicket(
            @NotBlank(message = "The id cannot be blank")
            @PathVariable final Integer ticketId,
            final Authentication authentication) {
        return new ResponseEntity<>(ticketService.getTicketByUser(authentication, ticketId), HttpStatus.OK);
    }

    @GetMapping(value = "my/")
    public ResponseEntity<List<TicketDto>> getOwnTickets(final Authentication authentication,
                                                         @RequestParam(required = false, defaultValue = "default") String sort,
                                                         @RequestParam(required = false, defaultValue = "") String filter) {
        return ResponseEntity.ok(ticketService.getTicketsByAuthentication(authentication, sort, true, filter));
    }

    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @PutMapping(value = "/")
    public ResponseEntity<OperationResultDto> addTicket(Authentication authentication,
                                                        @RequestBody TicketDto ticketDto) {
        OperationResultDto operationResultDto = ticketService.addTicket(authentication, ticketDto);
        return new ResponseEntity<>(operationResultDto, HttpStatus.CREATED);
    }

    @GetMapping(value = "/attachments/{id}")
    public ResponseEntity<List<AttachmentSimpleDto>> getTicketFilelist(@NotBlank(message = "The id cannot be blank")
                                                                       @PathVariable final Integer id) {
        return new ResponseEntity<>(ticketService.getTicketFilelist(id), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @PostMapping(value = "/{ticketId}")
    public ResponseEntity<Void> modifyTicket(Authentication authentication,
                                             @NotBlank(message = "The id cannot be blank") @PathVariable final Integer ticketId,
                                             @RequestBody TicketDto ticketDto) {
        ticketService.modifyTicket(authentication, ticketDto, ticketId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @GetMapping(value = "/permissions")
    public ResponseEntity<List<PermissionDto>> getPermissions(Authentication authData) {
        return new ResponseEntity<>(ticketService.getPermissions(authData), HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/cancelled/")
    public ResponseEntity<Void> cancelTicket(Authentication authData,
                                             @NotBlank(message = "The id cannot be blank")
                                             @PathVariable final Integer id) {
        ticketService.cancelTicket(id, authData);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/submitted/")
    public ResponseEntity<Void> submitTicket(Authentication authData,
                                             @NotBlank(message = "The id cannot be blank")
                                             @PathVariable final Integer id) {
        ticketService.submitTicket(id, authData);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/approved/")
    public ResponseEntity<Void> approveTicket(Authentication authData,
                                              @NotBlank(message = "The id cannot be blank")
                                              @PathVariable final Integer id) {
        ticketService.approveTicket(id, authData);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/declined/")
    public ResponseEntity<Void> declineTicket(Authentication authData,
                                              @NotBlank(message = "The id cannot be blank")
                                              @PathVariable final Integer id) {
        ticketService.declineTicket(id, authData);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/assigned/")
    public ResponseEntity<Void> assignToMeTicket(Authentication authData,
                                                 @NotBlank(message = "The id cannot be blank")
                                                 @PathVariable final Integer id) {
        ticketService.assignToMeTicket(id, authData);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/done/")
    public ResponseEntity<Void> setTicketDoneState(Authentication authData,
                                                   @NotBlank(message = "The id cannot be blank")
                                                   @PathVariable final Integer id) {
        ticketService.setTicketStateDone(id, authData);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/histories/{id}")
    public ResponseEntity<List<HistoryDto>> getTicketHistory(
            Authentication authData,
            @NotBlank(message = "The id cannot be blank") @PathVariable final Integer id,
            @RequestParam(required = false, defaultValue = "") String unlimited) {

        return new ResponseEntity<>(ticketService.getTicketHistory(authData, id, unlimited), HttpStatus.OK);
    }
}
