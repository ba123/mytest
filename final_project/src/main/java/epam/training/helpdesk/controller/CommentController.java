package epam.training.helpdesk.controller;

import epam.training.helpdesk.dto.comment.CommentDto;
import epam.training.helpdesk.service.CommentService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentController {

    private CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @PostMapping(value = "/")
    public ResponseEntity<Void> addComment(@RequestBody CommentDto dto, final Authentication authentication) {
        commentService.addComment(dto, authentication);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/tickets/{id}")
    public ResponseEntity<List<CommentDto>> getTicketComments(Authentication authentication,
                                                              @NotBlank(message = "The id cannot be blank")
                                                              @PathVariable final Integer id,
                                                              @RequestParam(required = false, defaultValue = "") String unlimited) {
        return ResponseEntity.ok(commentService.getTicketComments(id, unlimited, authentication));
    }
}
