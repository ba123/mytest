package epam.training.helpdesk.controller;

import epam.training.helpdesk.converter.UserConverter;
import epam.training.helpdesk.converter.impl.UserConverterImpl;
import epam.training.helpdesk.dto.user.UserDto;
import epam.training.helpdesk.service.UserService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/{userId}")
    public ResponseEntity<UserDto> getUser(
            @NotBlank(message = "The id cannot be blank")
            @PathVariable final Integer userId) {
        UserConverter converter = new UserConverterImpl();
        return new ResponseEntity<>(converter.convertToDto(userService.getUserById(userId)), HttpStatus.OK);
    }

}