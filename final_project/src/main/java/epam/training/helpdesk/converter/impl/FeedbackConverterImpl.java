package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.FeedbackConverter;
import epam.training.helpdesk.domain.Feedback;
import epam.training.helpdesk.dto.feedback.FeedbackDto;
import org.springframework.stereotype.Component;


@Component
public class FeedbackConverterImpl implements FeedbackConverter {

    private static final String EMPTY_STRING = "";

    public Feedback convertFromDto(FeedbackDto dto) {
        Feedback feedback = new Feedback(dto.getRate(), dto.getText());
        if (feedback.getText() == null) {
            feedback.setText(EMPTY_STRING);
        }
        return feedback;
    }

    public FeedbackDto convertToDto(Feedback feedback) {
        return new FeedbackDto(feedback.getRate(), feedback.getText());
    }

}
