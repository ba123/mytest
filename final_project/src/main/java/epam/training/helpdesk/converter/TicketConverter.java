package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.dto.ticket.TicketDto;

/**
 * Ticket converter.
 */
public interface TicketConverter {
    /**
     * Convert Ticket object to dto.
     *
     * @param ticket ticket to convert into the dto.
     * @return TicketDto.
     */
    TicketDto convertToDto(Ticket ticket);

    /**
     * Convert Ticket DTO to the Ticket object.
     * Convert based on existing ("base") Ticket object.
     * Converter modifies base ticket object.
     *
     * @param ticketDto  Ticket DTO object to convert
     * @param baseTicket Base Ticket object.
     */
    void convertFromDto(TicketDto ticketDto, Ticket baseTicket);
}
