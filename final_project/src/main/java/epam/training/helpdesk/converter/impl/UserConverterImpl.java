package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.UserConverter;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.user.UserDto;

public class UserConverterImpl implements UserConverter {
    public UserDto convertToDto(User user) {
        return new UserDto(user.getId(), user.getFirstName(), user.getLastName());
    }
}
