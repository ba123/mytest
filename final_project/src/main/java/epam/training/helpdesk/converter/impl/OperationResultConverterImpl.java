package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.OperationResultConverter;
import epam.training.helpdesk.domain.OperationResult;
import epam.training.helpdesk.dto.operation.OperationResultDto;

public class OperationResultConverterImpl implements OperationResultConverter {
    @Override
    public OperationResultDto convertToDto(OperationResult changeResult) {
        return new OperationResultDto(changeResult.isOperationComplete(),
                changeResult.getId());
    }

}
