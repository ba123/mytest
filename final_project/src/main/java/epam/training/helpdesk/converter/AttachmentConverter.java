package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.dto.attachment.AttachmentDto;

public interface AttachmentConverter {
    AttachmentDto convertToDto(Attachment attachment);

    Attachment convertFromDto(AttachmentDto attachmentDto);
}
