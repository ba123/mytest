package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.TicketConverter;
import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.domain.Category;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.dto.ticket.TicketDto;
import epam.training.helpdesk.exception.general.OperationFailedException;
import epam.training.helpdesk.service.AttachmentService;
import epam.training.helpdesk.service.CategoryService;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

/**
 * Ticket converter implementation.
 */
@Component
public class TicketConverterImpl implements TicketConverter {
    private static final String DATA_IS_INVALID_MESSAGE = "Data is invalid";
    private static final String ATTACHMENT_LINKING_FAILED = "Cannot link attachment to the ticket";

    private CategoryService categoryService;


    private AttachmentService attachmentService;

    public TicketConverterImpl(CategoryService categoryService, AttachmentService attachmentService) {
        this.categoryService = categoryService;
        this.attachmentService = attachmentService;
    }

    /**
     * Convert Ticket object to dto.
     *
     * @param ticket ticket to convert into the dto.
     * @return TicketDto.
     */
    @Override
    public final TicketDto convertToDto(final Ticket ticket) {
        TicketDto ticketDto = new TicketDto();
        ticketDto.setId(ticket.getId());
        ticketDto.setCreatedOn(ticket.getCreatedOn());
        ticketDto.setName(ticket.getName());
        ticketDto.setDescription(ticket.getDescription());
        ticketDto.setDesiredResolutionDate(ticket.getDesiredResolutionDate());
        ticketDto.setOwnerId(ticket.getOwner().getId());
        ticketDto.setState(ticket.getState());
        ticketDto.setUrgency(ticket.getUrgency());
        if (ticket.getCategory() != null) {
            ticketDto.setCategoryId(ticket.getCategory().getId());
        }
        if (ticket.getApprover() != null) {
            ticketDto.setApproverId(ticket.getApprover().getId());
        }
        if (ticket.getAssignee() != null) {
            ticketDto.setAssigneeId(ticket.getAssignee().getId());
        }
        Set<Integer> attachmentsId = new HashSet<>();
        Set<Attachment> attachments = ticket.getAttachments();
        for (Attachment attachment : attachments) {
            attachmentsId.add(attachment.getId());
        }
        ticketDto.setAttachmentsId(attachmentsId);
        return ticketDto;
    }

    private Set<Attachment> getAttachmets(Set<Integer> attachmets) {
        Set<Attachment> newAttachmentSet = new HashSet<>();
        for (Integer attachmentId : attachmets) {
            newAttachmentSet.add(attachmentService.getAttachment(attachmentId).orElseThrow(() -> new NoSuchElementException(ATTACHMENT_LINKING_FAILED)));
        }
        return newAttachmentSet;
    }

    /**
     * Convert Ticket DTO to the Ticket object.
     * Convert based on existing ("base") Ticket object.
     * Converter modifies base ticket object.
     *
     * @param ticketDto  Ticket DTO object to convert
     * @param baseTicket Base Ticket object.
     */
    @Override
    public void convertFromDto(TicketDto ticketDto, Ticket baseTicket) {
        baseTicket.setUrgency(ticketDto.getUrgency());
        baseTicket.setName(ticketDto.getName());
        baseTicket.setDescription(ticketDto.getDescription());
        baseTicket.setState(ticketDto.getState());
        if (ticketDto.getCategoryId() != null) {
            Optional<Category> category = categoryService.getCategory(ticketDto.getCategoryId());
            baseTicket.setCategory(category.orElseThrow(() -> new OperationFailedException(DATA_IS_INVALID_MESSAGE)));
        }
        if (ticketDto.getAttachmentsId() == null) {
            baseTicket.setAttachments(new HashSet<>());
        } else {
            baseTicket.setAttachments(getAttachmets(ticketDto.getAttachmentsId()));
        }
         baseTicket.setDesiredResolutionDate(ticketDto.getDesiredResolutionDate());
    }
}
