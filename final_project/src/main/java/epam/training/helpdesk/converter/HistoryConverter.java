package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.History;
import epam.training.helpdesk.dto.history.HistoryDto;

public interface HistoryConverter {
    HistoryDto convertToDto(History history);
}
