package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.OperationResult;
import epam.training.helpdesk.dto.operation.OperationResultDto;

public interface OperationResultConverter {
    OperationResultDto convertToDto(OperationResult changeResult);
}
