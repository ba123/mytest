package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.PermissionConverter;
import epam.training.helpdesk.domain.Permission;
import epam.training.helpdesk.dto.permission.PermissionDto;
import org.springframework.stereotype.Component;

@Component
public class PermissionConverterImpl implements PermissionConverter {
    public PermissionDto convertToDto(Permission permission, Integer ticketId) {
        return new PermissionDto(ticketId,
                permission.getSubmitAllowed(),
                permission.getCancelAllowed(),
                permission.getApproveAllowed(),
                permission.getDeclineAllowed(),
                permission.getAssignToMeAllowed(),
                permission.getDoneAllowed(),
                permission.getFeedbackViewingAllowed(),
                permission.getFeedbackAdditionAllowed());
    }
}
