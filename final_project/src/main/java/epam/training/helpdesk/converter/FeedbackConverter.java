package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.Feedback;
import epam.training.helpdesk.dto.feedback.FeedbackDto;

public interface FeedbackConverter {
    Feedback convertFromDto(FeedbackDto dto);

    FeedbackDto convertToDto(Feedback feedback);
}
