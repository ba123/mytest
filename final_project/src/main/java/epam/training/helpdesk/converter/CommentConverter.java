package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.Comment;
import epam.training.helpdesk.dto.comment.CommentDto;

public interface CommentConverter {
    Comment convertFromDto(CommentDto dto);

    CommentDto convertToDto(Comment comment);
}
