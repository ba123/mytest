package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.CommentConverter;
import epam.training.helpdesk.domain.Comment;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.comment.CommentDto;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.service.TicketService;
import epam.training.helpdesk.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class CommentConverterImpl implements CommentConverter {
    private static final String CONVERTATION_FAILED_MESSAGE = "Convertation failed.";

    UserService userService;
    TicketService ticketService;

    public CommentConverterImpl(UserService userService, TicketService ticketService) {
        this.userService = userService;
        this.ticketService = ticketService;
    }

    public Comment convertFromDto(CommentDto dto) {
        Comment comment = new Comment();
        comment.setText(dto.getText());
        User user = userService.getUserById(dto.getUserId());
        Ticket ticket = ticketService.getTicket(dto.getTicketId())
                .orElseThrow(() -> new DataNotFoundException(CONVERTATION_FAILED_MESSAGE));
        comment.setCommentator(user);
        comment.setCommentedTicket(ticket);
        comment.setDate(dto.getDate());
        return comment;
    }

    public CommentDto convertToDto(Comment comment) {
        String username = comment.getCommentator().getFirstName() + " " + comment.getCommentator().getLastName();
        CommentDto dto = new CommentDto(comment.getId(), comment.getText(), comment.getDate(), username);
        return dto;
    }
}
