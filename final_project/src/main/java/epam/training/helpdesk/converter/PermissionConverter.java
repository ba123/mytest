package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.Permission;
import epam.training.helpdesk.dto.permission.PermissionDto;

public interface PermissionConverter {
    PermissionDto convertToDto(Permission permission, Integer ticketId);
}
