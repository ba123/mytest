package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.user.UserDto;

public interface UserConverter {
    UserDto convertToDto(User user);
}
