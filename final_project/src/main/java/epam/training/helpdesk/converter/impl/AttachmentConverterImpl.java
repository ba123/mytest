package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.AttachmentConverter;
import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.dto.attachment.AttachmentDto;

public class AttachmentConverterImpl implements AttachmentConverter {

    public AttachmentDto convertToDto(Attachment attachment) {
        return new AttachmentDto(attachment.getId(),
                attachment.getBlob(),
                attachment.getName());
    }

    public Attachment convertFromDto(AttachmentDto attachmentDto) {
        return new Attachment(attachmentDto.getFileBody(), attachmentDto.getName());

    }
}
