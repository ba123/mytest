package epam.training.helpdesk.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Application initializer.
 */
public class ApplicationInitializer extends
        AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * Get root config classes.
     *
     * @return Array of the root config classes.
     */
    @Override
    protected final Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{AppConfig.class};
    }

    /**
     * Get servlet config classes.
     *
     * @return servlet config classes.
     */
    @Override
    protected final Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{};
    }


    /**
     * Get servlet mappings.
     *
     * @return Servlet mappings (string array).
     */
    @Override
    protected final String[] getServletMappings() {
        return new String[]{"/"};
    }

}
