package epam.training.helpdesk.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;

/**
 * Hibernate configuration class.
 */

@PropertySource("classpath:db/db.properties")
@Configuration
@ComponentScan(basePackages = "epam.training.helpdesk")
@EnableTransactionManagement
public class HibernateConfiguration {

    /**
     * Hibernate dialect to use.
     */
    @Value("${db.hibernate.dialect}")
    private String hibernateDialect;

    /**
     * database connection url.
     */
    @Value("${db.url}")
    private String dbUrl;

    /**
     * database connection password.
     */
    @Value("${db.password}")
    private String dbPassword;

    /**
     * Database connection username.
     */
    @Value("${db.username}")
    private String dbUsername;


    /**
     * Database init script resource.
     */
    @Value("${db.initscript}")
    private Resource dbInitScript;

    /**
     * Database driver.
     */
    @Value("${db.driver}")
    private String dbDriverName;

    /**
     * Datasource manager.
     *
     * @return DriverManagerDataSource object.
     */
    @Bean
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dbDriverName);
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);

        return dataSource;
    }


    /**
     * Database populator.
     *
     * @return New databasePopulator object.
     */
    @Bean
    public DatabasePopulator databasePopulator() {
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        populator.setScripts(dbInitScript);
        return populator;
    }

    /**
     * Datasource initializer.
     *
     * @return new DataSourceInitializer object.
     */
    @Bean
    public DataSourceInitializer dataSourceInitializer() {
        DataSourceInitializer initializer = new DataSourceInitializer();
        initializer.setDataSource(dataSource());
        initializer.setDatabasePopulator(databasePopulator());
        return initializer;
    }

    /**
     * Session factory for Hibernate.
     *
     * @return LocalSessionFactoryBean to use.
     */
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(
                new String[]{"epam.training.helpdesk"});
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }

    /**
     * Return Transaction Manager.
     *
     * @return PlatformTransactionManager object.
     */
    @Bean
    public PlatformTransactionManager hibernateTransactionManager() {
        HibernateTransactionManager transactionManager
                = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    /**
     * Return Hibernate properties.
     *
     * @return Properties for Hibernate.
     */
    private Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty(
                "hibernate.hbm2ddl.auto", "none");
        hibernateProperties.setProperty(
                "hibernate.connection.pool_size", "10");
        hibernateProperties.setProperty(
                "hibernate.dialect", hibernateDialect);
        return hibernateProperties;
    }


}
