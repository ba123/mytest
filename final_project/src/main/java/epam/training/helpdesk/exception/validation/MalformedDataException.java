package epam.training.helpdesk.exception.validation;

public class MalformedDataException extends RuntimeException {
    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public MalformedDataException(final String message) {
        super(message);
    }
}
