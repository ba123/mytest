package epam.training.helpdesk.exception.data;

public class DataNotFoundException extends RuntimeException {
    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public DataNotFoundException(final String message) {
        super(message);
    }
}
