package epam.training.helpdesk.exception.notification;

public class NotificationFailedException extends RuntimeException {
    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public NotificationFailedException(final String message) {
        super(message);
    }
}
