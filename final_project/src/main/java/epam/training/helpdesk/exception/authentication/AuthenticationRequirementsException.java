package epam.training.helpdesk.exception.authentication;

/**
 * Exception to be thrown when authentication parameters has bad format.
 */

public class AuthenticationRequirementsException extends RuntimeException {
    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public AuthenticationRequirementsException(final String message) {
        super(message);
    }


}