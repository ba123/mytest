package epam.training.helpdesk.exception.handler;

import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Class to represent exception's messages.
 */
public class ApiError {
    /**
     * Http status code.
     */
    private HttpStatus status;
    /**
     * Exception message string.
     */
    private String message;
    /**
     * Message errors list.
     */
    private List<String> errors;

    /**
     * Constructor.
     *
     * @param statusToUse  http request status code.
     * @param messageToUse message to display.
     * @param errorsToUse  error lines to display.
     */
    public ApiError(final HttpStatus statusToUse, final String messageToUse,
                    final List<String> errorsToUse) {
        status = statusToUse;
        message = messageToUse;
        errors = errorsToUse;
    }

    /**
     * Constructor.
     *
     * @param statusToUse  http request status code.
     * @param messageToUse message to display.
     * @param errorToUse   error lines to display.
     */
    public ApiError(final HttpStatus statusToUse, final String messageToUse,
                    final String errorToUse) {
        status = statusToUse;
        message = messageToUse;
        errors = Arrays.asList(errorToUse);
    }

    /**
     * Get http request status.
     *
     * @return http request status.
     */
    public final HttpStatus getStatus() {
        return status;
    }

    /**
     * Get exception message.
     *
     * @return exception message.
     */
    public final String getMessage() {
        return message;
    }

    /**
     * Get errors list.
     *
     * @return Errors list.
     */
    public final List<String> getErrors() {
        return errors;
    }
}
