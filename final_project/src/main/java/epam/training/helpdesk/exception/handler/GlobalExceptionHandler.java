package epam.training.helpdesk.exception.handler;

import epam.training.helpdesk.exception.authentication.UserNotFoundException;
import epam.training.helpdesk.exception.authorization.AccessDeniedException;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.exception.general.OperationFailedException;
import epam.training.helpdesk.exception.notification.NotificationFailedException;
import epam.training.helpdesk.exception.validation.ValidationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.persistence.NoResultException;

/**
 * Global exception handler.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * IllegalArgumentException handler.
     *
     * @param ex exception
     * @return response entity to send corresponding message.
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public final ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            final RuntimeException ex) {
        ApiError apiError =
                new ApiError(HttpStatus.BAD_REQUEST, "Bad argument",
                        ex.getMessage());
        return new ResponseEntity<>(
                apiError, new HttpHeaders(), apiError.getStatus());
    }

    /**
     * NoHandlerFoundException  handler (Page not found).
     *
     * @return response entity to send corresponding message.
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    public final ResponseEntity<Object> handleIOException() {
        ApiError apiError =
                new ApiError(HttpStatus.NOT_FOUND, "Page not found", "");
        return new ResponseEntity<>(
                apiError, new HttpHeaders(), apiError.getStatus());
    }

    /**
     * AccessDeniedException handler.
     *
     * @param ex exception
     * @return response entity to send corresponding message.
     */
    @ExceptionHandler(AccessDeniedException.class)
    public final ResponseEntity<Object> handleAccessDeniedException(
            final RuntimeException ex) {
        ApiError apiError =
                new ApiError(HttpStatus.BAD_REQUEST, "Access denied", ex.getMessage());
        return new ResponseEntity<>(
                apiError, new HttpHeaders(), apiError.getStatus());
    }


    /**
     * ValidationException handler.
     *
     * @param ex exception
     * @return response entity to send corresponding message.
     */
    @ExceptionHandler(ValidationException.class)
    public final ResponseEntity<Object> handleValidationException(final RuntimeException ex) {
        ApiError apiError =
                new ApiError(HttpStatus.BAD_REQUEST, "Validation Failed", ex.getMessage());
        return new ResponseEntity<>(
                apiError, new HttpHeaders(), apiError.getStatus());
    }

    /**
     * DataNotFoundException handler.
     *
     * @param ex exception
     * @return response entity to send corresponding message.
     */
    @ExceptionHandler(DataNotFoundException.class)
    public final ResponseEntity<Object> handleDataNotFoundException(final RuntimeException ex) {
        ApiError apiError =
                new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Data not found", ex.getMessage());
        return new ResponseEntity<>(
                apiError, new HttpHeaders(), apiError.getStatus());
    }


    /**
     * OperationFailedException handler.
     *
     * @param ex exception
     * @return response entity to send corresponding message.
     */
    @ExceptionHandler(OperationFailedException.class)
    public final ResponseEntity<Object> handleOperationFailedException(final RuntimeException ex) {
        ApiError apiError =
                new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Operation failed", ex.getMessage());
        return new ResponseEntity<>(
                apiError, new HttpHeaders(), apiError.getStatus());
    }

    /**
     * UserNotFoundException handler.
     *
     * @param ex exception
     * @return response entity to send corresponding message.
     */
    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<Object> handleUserNotFoundException(
            final RuntimeException ex) {
        ApiError apiError =
                new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "User not "
                        + "found", ex.getMessage());
        return new ResponseEntity<>(
                apiError, new HttpHeaders(), apiError.getStatus());
    }

    /**
     * NoResultException handler.
     *
     * @param ex exception
     * @return response entity to send corresponding message.
     */
    @ExceptionHandler(NoResultException.class)
    public final ResponseEntity<Object> handleNoResultException(
            final RuntimeException ex) {
        ApiError apiError =
                new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Requested or required information not found", ex.getMessage());
        return new ResponseEntity<>(
                apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler(NotificationFailedException.class)
    public final ResponseEntity<Object> handleNotificationFailedException(
            final RuntimeException ex) {
        ApiError apiError =
                new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot send email", ex.getMessage());
        return new ResponseEntity<>(
                apiError, new HttpHeaders(), apiError.getStatus());
    }

}
