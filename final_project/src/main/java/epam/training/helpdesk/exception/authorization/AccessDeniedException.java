package epam.training.helpdesk.exception.authorization;

/**
 * Access denied exception.
 */
public class AccessDeniedException extends RuntimeException {
    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public AccessDeniedException(final String message) {
        super(message);
    }
}
