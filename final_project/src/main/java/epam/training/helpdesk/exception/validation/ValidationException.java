package epam.training.helpdesk.exception.validation;

public class ValidationException extends RuntimeException {
    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public ValidationException(final String message) {
        super(message);
    }
}