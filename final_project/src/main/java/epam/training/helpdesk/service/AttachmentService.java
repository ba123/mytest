package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.attachment.AttachmentDto;
import epam.training.helpdesk.dto.operation.OperationResultDto;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

public interface AttachmentService {
    Optional<Attachment> getAttachment(Integer attachmentId, User requesterUser);

    Optional<Attachment> getAttachment(Integer attachmentId);

    Optional<Attachment> addAttachment(Attachment attachment);

    void updateAttachment(Attachment attachment);

    void linkAttachmentToTicket(Integer attachmentId, Ticket ticket);

    void update(Attachment attachment);

    void deleteAttachment(Integer id);

    void deleteAttachmentChecked(Integer id);

    AttachmentDto getAttachmentDto(Integer id, final Authentication authentication);

    OperationResultDto uploadNewFile(MultipartFile multipartFile) throws IOException;

}
