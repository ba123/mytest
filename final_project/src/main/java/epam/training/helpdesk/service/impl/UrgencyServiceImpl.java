package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.domain.Urgency;
import epam.training.helpdesk.service.UrgencyService;
import org.springframework.stereotype.Service;

@Service
public class UrgencyServiceImpl implements UrgencyService {
    public Urgency[] getUrgencyList() {
        return Urgency.values();
    }
}
