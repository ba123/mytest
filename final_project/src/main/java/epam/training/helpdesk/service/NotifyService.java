package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.User;

public interface NotifyService {
    void notifyTicketChangedStateToNew(Integer ticketId);

    void notifyTicketApproved(Integer ticketId, User ticketOwner);

    void notifyTicketDeclined(Integer ticketId, User ticketOwner);

    void notifyTicketCancelledByManager(Integer ticketId, User ticketOwner);

    void notifyTicketCancelledByEngineer(Integer ticketId, User ticketOwner, User ticketAppover);

    void notifyTicketDone(Integer ticketId, User ticketOwner);

    void notifyTicketFeedbackProvided(Integer ticketId, User ticketAssignee);

}
