package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class UserDetailsServiceImpl implements UserDetailsService {
    /**
     * User repository to use (to authenticate users using database).
     */
    private final UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Get user object in the database by username.
     *
     * @param userName username to search
     * @return User object.
     * @throws UsernameNotFoundException if user not found.
     */
    @Override
    public UserDetails loadUserByUsername(final String userName) {
        return userRepository.getUserByLogin(userName).orElseThrow(() ->
                new UsernameNotFoundException("Username not found "));
    }
}
