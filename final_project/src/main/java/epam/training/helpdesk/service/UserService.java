package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.User;

import java.util.List;

/**
 * User service interface.
 */
public interface UserService {
    /**
     * Get user object by login name.
     * If user doesn't exist then method creates new user object.
     *
     * @param loginName user login name
     * @return User object with required login.
     */
    Integer getUserIdByLoginName(String loginName);

    User getUserById(Integer id);

    List<User> getManagers();

    List<User> getEngineers();
}
