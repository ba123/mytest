package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.Permission;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.attachment.AttachmentSimpleDto;
import epam.training.helpdesk.dto.history.HistoryDto;
import epam.training.helpdesk.dto.operation.OperationResultDto;
import epam.training.helpdesk.dto.permission.PermissionDto;
import epam.training.helpdesk.dto.ticket.TicketDto;
import org.springframework.security.core.Authentication;

import java.util.List;
import java.util.Optional;

public interface TicketService {
    List<Ticket> getAllTickets();

    List<TicketDto> getTicketsByAuthentication(Authentication authentication, String sortOrderParam, Boolean personalTicketsOnly, String filter);

    List<Integer> getTicketsIdByUser(User user);

    boolean isTicketVisible(Integer ticketId, User user);

    Optional<Ticket> getTicketChecked(Integer ticketId, User user);

    Optional<Ticket> addTicket(Ticket ticket);

    OperationResultDto addTicket(Authentication authentication, TicketDto ticketDto);

    void modifyTicket(Authentication authentication, TicketDto ticketDto, Integer ticketId);

    Optional<Ticket> getTicket(Integer id);

    TicketDto getTicketByUser(Authentication authData, Integer ticketId);

    List<AttachmentSimpleDto> getTicketFilelist(Integer id);

    List<PermissionDto> getPermissions(Authentication authData);

    void cancelTicket(Integer ticketId, Authentication authData);

    void submitTicket(Integer ticketId, Authentication authData);

    void approveTicket(Integer ticketId, Authentication authData);

    void declineTicket(Integer ticketId, Authentication authData);

    void assignToMeTicket(Integer ticketId, Authentication authData);

    void setTicketStateDone(Integer ticketId, Authentication authData);

    List<HistoryDto> getTicketHistory(Authentication authData, Integer id, String unlimited);

    Permission getAllowedActions(Integer ticketId, User user);

}
