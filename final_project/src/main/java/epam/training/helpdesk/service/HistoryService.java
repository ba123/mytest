package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.Action;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.history.HistoryDto;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface HistoryService {
    List<HistoryDto> getTicketHistory(final Integer id, String limit, Authentication authData);

    void addHistory(User user, Ticket ticket, Action action, String description);
}
