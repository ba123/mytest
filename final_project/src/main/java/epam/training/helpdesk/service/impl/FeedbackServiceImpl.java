package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.converter.FeedbackConverter;
import epam.training.helpdesk.domain.Feedback;
import epam.training.helpdesk.domain.Permission;
import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.feedback.FeedbackDto;
import epam.training.helpdesk.exception.authorization.AccessDeniedException;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.repository.FeedbackRepository;
import epam.training.helpdesk.service.FeedbackService;
import epam.training.helpdesk.service.NotifyService;
import epam.training.helpdesk.service.TicketService;
import epam.training.helpdesk.validator.controller.feedback.FeedbackValidator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Optional;

@Service
@Transactional
public class FeedbackServiceImpl implements FeedbackService {
    private static final String TICKET_NOT_FOUND_MESSAGE = "Ticket not found";
    private static final String NO_VISIBLE_TICKET_MESSAGE = "Request is denied or corresponding ticket doesn't exist.";

    private final FeedbackConverter converter;
    private final FeedbackValidator validator;
    private final TicketService ticketService;
    private final FeedbackRepository feedbackRepository;
    private final NotifyService notifyService;

    public FeedbackServiceImpl(FeedbackConverter converter, FeedbackValidator validator, TicketService ticketService, FeedbackRepository feedbackRepository, NotifyService notifyService) {
        this.converter = converter;
        this.validator = validator;
        this.ticketService = ticketService;
        this.feedbackRepository = feedbackRepository;
        this.notifyService = notifyService;
    }

    @Override
    public Optional<Feedback> addFeedback(FeedbackDto dto, Authentication authData, Integer ticketId) {
        validator.validate(dto);
        User user = (User) authData.getPrincipal();
        Permission permission = ticketService.getAllowedActions(ticketId, user);
        if (!permission.getFeedbackAdditionAllowed()) {
            throw new AccessDeniedException(NO_VISIBLE_TICKET_MESSAGE);
        }
        Feedback feedback = converter.convertFromDto(dto);
        feedback.setDate(new Timestamp(System.currentTimeMillis()));
        feedback.setFeedbackAuthor((User) authData.getPrincipal());
        Ticket ticket = ticketService.getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        State prevState = ticket.getState();
        feedback.setFeedbackedTicket(ticket);
        feedback.setFeedbackAuthor(user);
        Optional<Feedback> result = feedbackRepository.addFeedback(feedback);
        if (prevState == State.DONE) {
            notifyService.notifyTicketFeedbackProvided(ticketId, ticket.getAssignee());
        }
        return result;
    }

    @Override
    public FeedbackDto getFeedback(Authentication authData, Integer ticketId) {
        User user = (User) authData.getPrincipal();
        Permission permission = ticketService.getAllowedActions(ticketId, user);
        if (!permission.getFeedbackViewingAllowed()) {
            throw new AccessDeniedException(NO_VISIBLE_TICKET_MESSAGE);
        }
        Feedback feedback = feedbackRepository.getFeedback(ticketId);
        return converter.convertToDto(feedback);
    }
}
