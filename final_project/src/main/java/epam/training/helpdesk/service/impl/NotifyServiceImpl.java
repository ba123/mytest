package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.exception.notification.NotificationFailedException;
import epam.training.helpdesk.service.NotifyService;
import epam.training.helpdesk.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@PropertySource("classpath:notifier.properties")
public class NotifyServiceImpl implements NotifyService {

    @Value("${frontend.ticketOvervew.url}")
    private String ticketOverviewUrl;

    private TemplateEngine emailTemplateEngine;
    private JavaMailSender mailSender;
    private ResourceBundleMessageSource emailMessageSource;
    private TemplateEngine htmlTemplateEngine;
    private UserService userService;

    public NotifyServiceImpl(TemplateEngine emailTemplateEngine, JavaMailSender mailSender, ResourceBundleMessageSource emailMessageSource, TemplateEngine htmlTemplateEngine, UserService userService) {
        this.emailTemplateEngine = emailTemplateEngine;
        this.mailSender = mailSender;
        this.emailMessageSource = emailMessageSource;
        this.htmlTemplateEngine = htmlTemplateEngine;
        this.userService = userService;
    }

    private void sendCustomNotification(final User user, Integer ticketId,
                                        String subject,
                                        String template) {

//        final Context ctx = new Context(Locale.getDefault());
//        ctx.setVariable("ticketUrl", ticketOverviewUrl + ticketId);
//        ctx.setVariable("targetEmail", user.getEmail());
//        ctx.setVariable("firstName", user.getFirstName());
//        ctx.setVariable("lastName", user.getLastName());
//        ctx.setVariable("ticketId", ticketId);
//
//        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
//        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
//        final String htmlContent = this.htmlTemplateEngine.process(template, ctx);
//        try {
//            message.setSubject(subject);
//            //message.setTo(user.getEmail());
//            message.setTo("javatraining20192019@gmail.com");
//            message.setText(htmlContent, true /* isHtml */);
//        } catch (MessagingException ex) {
//            throw new NotificationFailedException("Cannot send email to " + user.getEmail());
//        }
//        this.mailSender.send(mimeMessage);
    }

    private void sendNotificationStateChangedToNew(User user, Integer ticketId) {
        sendCustomNotification(user, ticketId, "New ticket for approval", "html/template01.html");
    }

    @Override
    public void notifyTicketChangedStateToNew(Integer ticketId) {
        List<User> managers = userService.getManagers();
        for (User user : managers) {
            sendNotificationStateChangedToNew(user, ticketId);
        }
    }

    @Override
    public void notifyTicketApproved(Integer ticketId, User ticketOwner) {
        List<User> recipients = userService.getEngineers();
        recipients.add(ticketOwner);
        for (User user : recipients) {
            sendCustomNotification(user, ticketId, "Ticket was approved", "html/template02.html");
        }
    }

    @Override
    public void notifyTicketDeclined(Integer ticketId, User ticketOwner) {
        sendCustomNotification(ticketOwner, ticketId, "Ticket was declined", "html/template03.html");
    }

    @Override
    public void notifyTicketCancelledByManager(Integer ticketId, User ticketOwner) {
        sendCustomNotification(ticketOwner, ticketId, "Ticket was cancelled", "html/template04.html");
    }

    @Override
    public void notifyTicketCancelledByEngineer(Integer ticketId, User ticketOwner, User ticketAppover) {
        List<User> recipients = new ArrayList<>();
        recipients.add(ticketOwner);
        if (!ticketOwner.equals(ticketAppover)) {
            recipients.add(ticketAppover);
        }
        for (User user : recipients) {
            sendCustomNotification(user, ticketId, "Ticket was cancelled", "html/template05.html");
        }
    }

    @Override
    public void notifyTicketDone(Integer ticketId, User ticketOwner) {
        sendCustomNotification(ticketOwner, ticketId, "Ticket was done", "html/template06.html");
    }

    @Override
    public void notifyTicketFeedbackProvided(Integer ticketId, User ticketAssignee) {
        sendCustomNotification(ticketAssignee, ticketId, "Feedback was provided", "html/template07.html");
    }

}