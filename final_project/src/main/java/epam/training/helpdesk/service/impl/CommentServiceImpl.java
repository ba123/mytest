package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.converter.CommentConverter;
import epam.training.helpdesk.domain.Comment;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.comment.CommentDto;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.repository.CommentRepository;
import epam.training.helpdesk.service.CommentService;
import epam.training.helpdesk.service.TicketService;
import epam.training.helpdesk.validator.controller.comment.CommentValidator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

    private static final String TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED = "Ticket not found or access denied";
    private static final Integer DEFAULT_ITEMS_LIMIT = 5;

    private final CommentRepository commentRepository;
    private final CommentValidator validator;
    private final CommentConverter converter;
    private final TicketService ticketService;

    public CommentServiceImpl(CommentRepository commentRepository, CommentValidator validator, CommentConverter converter, TicketService ticketService) {
        this.commentRepository = commentRepository;
        this.validator = validator;
        this.converter = converter;
        this.ticketService = ticketService;
    }

    @Override
    public void addComment(CommentDto commentDto, Authentication authData) {
        User user = (User) authData.getPrincipal();
        commentDto.setDate(new Timestamp(System.currentTimeMillis()));
        commentDto.setUserId(user.getId());
        validator.validateDto(commentDto);
        Comment comment = converter.convertFromDto(commentDto);
        commentRepository.addComment(comment);
    }

    @Override
    public List<CommentDto> getTicketComments(final Integer id, String unlimitedMark, Authentication authData) {
        User user = (User) authData.getPrincipal();
        Integer limit = DEFAULT_ITEMS_LIMIT;
        if (unlimitedMark != null && !unlimitedMark.isEmpty()) {
            limit = 0;
        }
        if (!ticketService.isTicketVisible(id, user)) {
            throw new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED);
        }
        List<Comment> comments = commentRepository.getTicketComments(id, limit);
        return comments.stream()
                .map(converter::convertToDto)
                .collect(Collectors.toList());
    }
}
