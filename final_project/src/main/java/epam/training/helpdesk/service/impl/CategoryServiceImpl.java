package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.domain.Category;
import epam.training.helpdesk.repository.CategoryRepository;
import epam.training.helpdesk.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.getAllCategories();
    }

    @Override
    public Optional<Category> getCategory(Integer id) {
        return categoryRepository.getCategory(id);
    }
}