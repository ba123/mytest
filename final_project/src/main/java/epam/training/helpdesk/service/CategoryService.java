package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    List<Category> getAllCategories();

    Optional<Category> getCategory(Integer id);
}
