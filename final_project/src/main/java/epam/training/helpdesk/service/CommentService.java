package epam.training.helpdesk.service;

import epam.training.helpdesk.dto.comment.CommentDto;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface CommentService {
    void addComment(CommentDto commentDto, Authentication authData);

    List<CommentDto> getTicketComments(final Integer id, String limit, Authentication authData);
}
