package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.converter.OperationResultConverter;
import epam.training.helpdesk.converter.PermissionConverter;
import epam.training.helpdesk.converter.TicketConverter;
import epam.training.helpdesk.converter.impl.OperationResultConverterImpl;
import epam.training.helpdesk.domain.Action;
import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.domain.Comment;
import epam.training.helpdesk.domain.OperationResult;
import epam.training.helpdesk.domain.Permission;
import epam.training.helpdesk.domain.SortingOrder;
import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.attachment.AttachmentSimpleDto;
import epam.training.helpdesk.dto.history.HistoryDto;
import epam.training.helpdesk.dto.operation.OperationResultDto;
import epam.training.helpdesk.dto.permission.PermissionDto;
import epam.training.helpdesk.dto.ticket.TicketDto;
import epam.training.helpdesk.exception.authorization.AccessDeniedException;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.exception.general.OperationFailedException;
import epam.training.helpdesk.repository.TicketRepository;
import epam.training.helpdesk.service.AttachmentService;
import epam.training.helpdesk.service.HistoryService;
import epam.training.helpdesk.service.NotifyService;
import epam.training.helpdesk.service.TicketService;
import epam.training.helpdesk.validator.controller.filter.FilterValidator;
import epam.training.helpdesk.validator.controller.sort.SortOrderValidator;
import epam.training.helpdesk.validator.controller.sort.impl.SortOrderValidatorImpl;
import epam.training.helpdesk.validator.controller.ticket.TicketValidator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class TicketServiceImpl implements TicketService {

    private static final String TICKET_NOT_FOUND_MESSAGE = "Ticket not found";
    private static final String ERROR_CHANGE_ATTACHMENT_LINK = "Cannot change attachment state";
    private static final String ACCESS_DENIED_MESSAGE = "Access denied";
    private static final String TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED = "Ticket not found or access denied";

    /**
     * Ticket repository.
     */
    private final TicketRepository ticketRepository;

    private TicketConverter ticketConverter;
    private TicketValidator ticketValidator;
    private FilterValidator filterValidator;
    private AttachmentService attachmentService;
    private PermissionConverter permissionConverter;
    private HistoryService historyService;
    private NotifyService notifyService;

    public TicketServiceImpl(TicketRepository ticketRepository, TicketConverter ticketConverter, TicketValidator ticketValidator, FilterValidator filterValidator, AttachmentService attachmentService, PermissionConverter permissionConverter, HistoryService historyService, NotifyService notifyService) {
        this.ticketRepository = ticketRepository;
        this.ticketConverter = ticketConverter;
        this.ticketValidator = ticketValidator;
        this.filterValidator = filterValidator;
        this.attachmentService = attachmentService;
        this.permissionConverter = permissionConverter;
        this.historyService = historyService;
        this.notifyService = notifyService;
    }

    @Override
    public List<Ticket> getAllTickets() {
        return ticketRepository.getAllTickets();
    }

    private List<Ticket> getTicketsByUser(User user, SortingOrder sortingOrder, Boolean personalTicketsOnly, String filter) {

        return ticketRepository.getTicketsByUser(user, sortingOrder, personalTicketsOnly, filter);
    }


    @Override
    public boolean isTicketVisible(Integer ticketId, User user) {
        return ticketRepository.isTicketVisible(ticketId, user);
    }

    @Override
    public Optional<Ticket> getTicketChecked(Integer ticketId, User user) {
        return ticketRepository.getTicketChecked(ticketId, user);
    }

    @Override
    public List<Integer> getTicketsIdByUser(User user) {
        return ticketRepository.getAllowedTicketsId(user);
    }

    public Optional<Ticket> addTicket(Ticket ticket) {
        return ticketRepository.addTicket(ticket);
    }

    @Override
    public Optional<Ticket> getTicket(Integer id) {
        return ticketRepository.getTicket(id);
    }

    @Override
    public List<TicketDto> getTicketsByAuthentication(Authentication authentication, String sortOrderParam, Boolean personalTicketsOnly, String filter) {
        filterValidator.validate(filter);
        SortOrderValidator sortOrderValidator = new SortOrderValidatorImpl();
        SortingOrder sortingOrder = sortOrderValidator.validate(sortOrderParam);
        User user = (User) authentication.getPrincipal();
        List<Ticket> tickets = getTicketsByUser(user, sortingOrder, personalTicketsOnly, filter);
        return tickets.stream()
                .map(ticketConverter::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Link unlinked attachment to the ticket.
     *
     * @param attachmentId attachment id to link.
     * @param ticketId     ticket id to link with.
     */
    private void linkAttachment(Integer attachmentId, Integer ticketId, Authentication authentication) {
        Attachment attachment = attachmentService.getAttachment(attachmentId)
                .orElseThrow(() -> new DataNotFoundException(ERROR_CHANGE_ATTACHMENT_LINK));
        if (attachment.getAttachmentTicket() != null) {
            throw new AccessDeniedException(ERROR_CHANGE_ATTACHMENT_LINK);
        }
        Ticket ticket = ticketRepository.getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        Set<Attachment> ticketAttacmnents = ticket.getAttachments();
        ticketAttacmnents.add(attachment);
        ticket.setAttachments(ticketAttacmnents);
        ticketRepository.updateTicket(ticket);
        attachment.setAttachmentTicket(ticket);
        attachmentService.update(attachment);
        historyService.addHistory((User) authentication.getPrincipal(), ticket, Action.ACTION_FILE_ATTACHED, ": \"" + attachment.getName() + "\"");
    }

    /**
     * Add new ticket.
     *
     * @param authentication User's authentication data.
     * @param ticketDto      Ticket DTO
     * @return OperationResultDto with result ticket id.
     */
    @Override
    public OperationResultDto addTicket(Authentication authentication, TicketDto ticketDto) {
        User creator = (User) authentication.getPrincipal();
        ticketDto.setOwnerId(creator.getId());
        ticketValidator.validateDto(ticketDto);

        Ticket ticketFromDto = new Ticket();
        ticketConverter.convertFromDto(ticketDto, ticketFromDto);
        ticketFromDto.setOwner(creator);
        ticketFromDto.setCreatedOn(new Timestamp(System.currentTimeMillis()));
        if (ticketFromDto.getId() != null) {
            throw new AccessDeniedException("You can't set ticket number in a new ticket");
        }
        Set<Integer> attachmentIds = ticketDto.getAttachmentsId();
        ticketDto.setAttachmentsId(new HashSet<Integer>());
        Ticket ticket = addTicket(ticketFromDto).orElseThrow(() -> new OperationFailedException("Cannot create ticket"));
        historyService.addHistory(creator, ticket, Action.ACTION_TICKET_CREATED, "");

        linkAttachments(attachmentIds, ticket.getId(), authentication);
        Comment comment = new Comment();
        comment.setCommentedTicket(ticket);
        comment.setCommentator(creator);
        OperationResult operationResult = new OperationResult(true, ticket.getId());
        OperationResultConverter resultConverter = new OperationResultConverterImpl();
        return resultConverter.convertToDto(operationResult);
    }

    /**
     * Unlink attachment from the ticket.
     * Requested attachment will be deleted.
     * Attachment must not be linked to any ticket or it must be linked to the requested ticket.
     *
     * @param attachmentId attachment id to unlink
     * @param ticketId     ticket id.
     * @throws DataNotFoundException if requested items not found.
     * @throws AccessDeniedException if attachment belongs to another ticket.
     */
    private void unlinkAttachment(Integer attachmentId, Integer ticketId, Authentication authentication) {
        Attachment attachment = attachmentService.getAttachment(attachmentId)
                .orElseThrow(() -> new DataNotFoundException(ERROR_CHANGE_ATTACHMENT_LINK));
        if (attachment.getAttachmentTicket() != null && !attachment.getAttachmentTicket().getId().equals(ticketId)) {
            throw new AccessDeniedException(ERROR_CHANGE_ATTACHMENT_LINK);
        }
        Ticket ticket = ticketRepository.getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        Set<Attachment> ticketAttacmnents = ticket.getAttachments();
        ticketAttacmnents.remove(attachment);
        ticket.setAttachments(ticketAttacmnents);
        ticketRepository.updateTicket(ticket);
        attachmentService.deleteAttachment(attachmentId);
        historyService.addHistory((User) authentication.getPrincipal(), ticket, Action.ACTION_FILE_REMOVED, ": \"" + attachment.getName() + "\"");
    }

    private void linkAttachments(Set<Integer> attachments, Integer ticketId, Authentication authentication) {
        for (Integer attachmentId : attachments) {
            linkAttachment(attachmentId, ticketId, authentication);
        }
    }

    private void unlinkAttachments(Set<Integer> attachments, Integer ticketId, Authentication authentication) {
        for (Integer attachmentId : attachments) {
            unlinkAttachment(attachmentId, ticketId, authentication);
        }
    }

    @Override
    public void modifyTicket(Authentication authentication, TicketDto ticketDto, Integer ticketId) {
        boolean ticketSubmitted = false;
        User user = (User) authentication.getPrincipal();
        ticketValidator.validateDto(ticketDto);
        Ticket prevTicket = ticketRepository.getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        if (prevTicket.getState() != State.DRAFT || !prevTicket.getOwner().equals(user)) {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
//        Set<Integer> prevAttaches = new HashSet<>();
//        for (Attachment attachment : prevTicket.getAttachments()) {
//            prevAttaches.add(attachment.getId());
//        }
        Set<Integer> prevAttaches = new HashSet<>(ticketRepository.getTicketAttachmentsIds(ticketId));
        State newState = ticketDto.getState();
        if (newState == State.NEW) {
            ticketSubmitted = true;
            ticketDto.setState(State.DRAFT);
            notifyService.notifyTicketChangedStateToNew(ticketId);

        }
        Ticket modifiedTicket = ticketRepository.getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        ticketConverter.convertFromDto(ticketDto, modifiedTicket);
        Set<Integer> dtoIdAttaches = new HashSet<>(ticketDto.getAttachmentsId());
        Set<Integer> newAttaches = new HashSet<>(dtoIdAttaches);
        newAttaches.removeAll(prevAttaches);
        Set<Integer> attachesToDelete = new HashSet<>(prevAttaches);
        attachesToDelete.removeAll(dtoIdAttaches);
        linkAttachments(newAttaches, ticketId, authentication);
        unlinkAttachments(attachesToDelete, ticketId, authentication);
        Ticket ticket = ticketRepository.getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        ticket.setName(ticketDto.getName());
        ticketRepository.updateTicket(modifiedTicket);
        historyService.addHistory(user, ticket, Action.ACTION_TICKET_EDITED, "");
        if (ticketSubmitted) {
            changeTicketStatus(ticketId, authentication, State.NEW);
        }
    }

    @Override
    public TicketDto getTicketByUser(Authentication authData, Integer ticketId) {
        User user = (User) authData.getPrincipal();
        Ticket ticket = getTicketChecked(ticketId, user).orElseThrow(() -> new OperationFailedException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED));
        return ticketConverter.convertToDto(ticket);
    }

    @Override
    public List<AttachmentSimpleDto> getTicketFilelist(Integer id) {
        Ticket ticket = ticketRepository.getTicket(id).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        Set<Attachment> attachments = ticket.getAttachments();
        List<AttachmentSimpleDto> filelist = new ArrayList<>();
        for (Attachment attachment : attachments) {
            filelist.add(new AttachmentSimpleDto(attachment.getId(), attachment.getName()));
        }
        return filelist;
    }

    @Override
    public List<PermissionDto> getPermissions(Authentication authData) {
        User user = (User) authData.getPrincipal();
        List<Integer> visibleTicketsIds = getTicketsIdByUser(user);
        List<PermissionDto> dtoList = new ArrayList<>();
        for (Integer ticketId : visibleTicketsIds) {
            Permission permission = ticketRepository.getAllowedActions(ticketId, user);
            dtoList.add(permissionConverter.convertToDto(permission, ticketId));
        }
        return dtoList;
    }

    @Override
    public void cancelTicket(Integer ticketId, Authentication authData) {
        Permission permission = ticketRepository.getAllowedActions(ticketId, (User) authData.getPrincipal());
        if (permission.getCancelAllowed()) {
            Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
            State prevState = ticket.getState();
            changeTicketStatus(ticketId, authData, State.CANCELLED);
            if (prevState == State.NEW) {
                notifyService.notifyTicketCancelledByManager(ticketId, ticket.getOwner());
            } else if (prevState == State.APPROVED) {
                notifyService.notifyTicketCancelledByEngineer(ticketId, ticket.getOwner(), ticket.getApprover());
            }
        } else {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public void submitTicket(Integer ticketId, Authentication authData) {
        Permission permission = ticketRepository.getAllowedActions(ticketId, (User) authData.getPrincipal());
        Ticket ticket = ticketRepository.getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        ticketValidator.validate(ticket);
        if (permission.getSubmitAllowed()) {
            State prevState = ticket.getState();
            changeTicketStatus(ticketId, authData, State.NEW);
            if (prevState == State.DRAFT || prevState == State.DECLINED) {
                notifyService.notifyTicketChangedStateToNew(ticketId);
            }
        } else {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public void approveTicket(Integer ticketId, Authentication authData) {
        User user = (User) authData.getPrincipal();
        Permission permission = ticketRepository.getAllowedActions(ticketId, user);
        if (permission.getApproveAllowed()) {
            Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
            State prevState = ticket.getState();
            ticketRepository.updateApprover(ticketId, user);
            changeTicketStatus(ticketId, authData, State.APPROVED);
            if (prevState == State.NEW) {
                notifyService.notifyTicketApproved(ticketId, ticket.getOwner());
            }
        } else {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public void declineTicket(Integer ticketId, Authentication authData) {
        Permission permission = ticketRepository.getAllowedActions(ticketId, (User) authData.getPrincipal());
        if (permission.getDeclineAllowed()) {
            Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
            State prevState = ticket.getState();
            changeTicketStatus(ticketId, authData, State.DECLINED);
            if (prevState == State.NEW) {
                notifyService.notifyTicketDeclined(ticketId, ticket.getOwner());
            }

        } else {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public void assignToMeTicket(Integer ticketId, Authentication authData) {
        User user = (User) authData.getPrincipal();
        Permission permission = ticketRepository.getAllowedActions(ticketId, user);
        if (permission.getAssignToMeAllowed()) {
            ticketRepository.updateAssignee(ticketId, user);
            changeTicketStatus(ticketId, authData, State.IN_PROGRESS);
        } else {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public void setTicketStateDone(Integer ticketId, Authentication authData) {
        Permission permission = ticketRepository.getAllowedActions(ticketId, (User) authData.getPrincipal());
        if (permission.getDoneAllowed()) {
            Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
            State prevState = ticket.getState();
            changeTicketStatus(ticketId, authData, State.DONE);
            if (prevState == State.IN_PROGRESS) {
                notifyService.notifyTicketDone(ticketId, ticket.getOwner());
            }
        } else {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
    }

    private void changeTicketStatus(Integer ticketId, Authentication authData, State newState) {
        Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        State prevState = ticket.getState();
        ticketRepository.changeTicketStatus(ticketId, newState);
        historyService.addHistory((User) authData.getPrincipal(), ticket, Action.ACTION_TICKET_CHANGED,
                " from '" + prevState + "' to '" + newState + "'");
    }

    public List<HistoryDto> getTicketHistory(Authentication authData, Integer id, String unlimited) {
        User user = (User) authData.getPrincipal();
        getTicketsIdByUser(user);
        List<Integer> allowedToViewTickets = getTicketsIdByUser(user);
        if (!allowedToViewTickets.contains(id)) {
            throw new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED);
        }
        return historyService.getTicketHistory(id, unlimited, authData);
    }

    @Override
    public Permission getAllowedActions(Integer ticketId, User user) {
        return ticketRepository.getAllowedActions(ticketId, user);
    }
}
