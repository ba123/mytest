package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.Urgency;

public interface UrgencyService {
    Urgency[] getUrgencyList();
}

