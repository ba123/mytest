package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.Feedback;
import epam.training.helpdesk.dto.feedback.FeedbackDto;
import org.springframework.security.core.Authentication;

import java.util.Optional;

public interface FeedbackService {
    Optional<Feedback> addFeedback(FeedbackDto dto, Authentication authData, Integer ticketId);

    FeedbackDto getFeedback(Authentication authData, Integer ticketId);
}
