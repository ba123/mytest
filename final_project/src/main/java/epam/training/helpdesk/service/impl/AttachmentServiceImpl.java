package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.converter.AttachmentConverter;
import epam.training.helpdesk.converter.OperationResultConverter;
import epam.training.helpdesk.converter.impl.AttachmentConverterImpl;
import epam.training.helpdesk.converter.impl.OperationResultConverterImpl;
import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.domain.OperationResult;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.attachment.AttachmentDto;
import epam.training.helpdesk.dto.operation.OperationResultDto;
import epam.training.helpdesk.exception.authorization.AccessDeniedException;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.exception.general.OperationFailedException;
import epam.training.helpdesk.repository.AttachmentRepository;
import epam.training.helpdesk.repository.TicketRepository;
import epam.training.helpdesk.service.AttachmentService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@Service
@Transactional
public class AttachmentServiceImpl implements AttachmentService {

    private static final String ATTACHMENT_ACCESS_DENIED_MESSAGE = "Access to the attachment is denied";
    private static final String ATTACHMENT_NOT_FOUND_MESSAGE = "Attachment not found";
    private static final String ATTACHMENT_UNABLE_TO_SAVE_MESSAGE = "Cannot add attachment to the database";
    private static final String ATTACHMENT_UNABLE_UNLINK_LINKED_ATTACHMENT = "You cannot delete attachments from any created tickets using REST API.";

    private final AttachmentRepository attachmentRepository;
    private final TicketRepository ticketRepository;

    public AttachmentServiceImpl(AttachmentRepository attachmentRepository,
                                 TicketRepository ticketRepository) {
        this.attachmentRepository = attachmentRepository;
        this.ticketRepository = ticketRepository;
    }

    @Override
    public Optional<Attachment> getAttachment(Integer attachmentId, User requesterUser) {
        Optional<Attachment> queryResult = attachmentRepository.getAttachment(attachmentId);
        if (queryResult.isPresent()) {
            Integer ticketId = queryResult.get().getAttachmentTicket().getId();
            if (ticketId != null && !ticketRepository.isTicketVisible(ticketId, requesterUser)) {
                throw new AccessDeniedException(ATTACHMENT_ACCESS_DENIED_MESSAGE);
            }
        }
        return queryResult;
    }

    @Override
    public Optional<Attachment> getAttachment(Integer attachmentId) {
        return attachmentRepository.getAttachment(attachmentId);
    }

    @Override
    public Optional<Attachment> addAttachment(Attachment attachment) {
        return attachmentRepository.addAttachment(attachment);
    }

    @Override
    public void updateAttachment(Attachment attachment) {
        attachmentRepository.updateAttachment(attachment);
    }

    @Override
    public void linkAttachmentToTicket(Integer attachmentId, Ticket ticket) {
        Attachment attachment = getAttachment(attachmentId).orElseThrow(() -> new DataNotFoundException(ATTACHMENT_NOT_FOUND_MESSAGE));
        attachment.setAttachmentTicket(ticket);
    }

    @Override
    public void update(Attachment attachment) {
        attachmentRepository.updateAttachment(attachment);
    }

    @Override
    public void deleteAttachment(Integer id) {
        attachmentRepository.deleteAttachment(id);
    }

    @Override
    public void deleteAttachmentChecked(Integer id) {
        Attachment attachment = getAttachment(id).orElseThrow(() -> new DataNotFoundException(ATTACHMENT_NOT_FOUND_MESSAGE));
        if (attachment.getAttachmentTicket() == null) {
            attachmentRepository.deleteAttachment(id);
        } else {
            throw new AccessDeniedException(ATTACHMENT_UNABLE_UNLINK_LINKED_ATTACHMENT);
        }
    }

    @Override
    public AttachmentDto getAttachmentDto(Integer id, final Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        Attachment attachment = getAttachment(id, user)
                .orElseThrow(() -> new DataNotFoundException(ATTACHMENT_NOT_FOUND_MESSAGE));
        AttachmentConverter converter = new AttachmentConverterImpl();
        return converter.convertToDto(attachment);
    }

    @Override
    public OperationResultDto uploadNewFile(MultipartFile multipartFile) throws IOException {
        AttachmentDto dto = new AttachmentDto();
        dto.setName(multipartFile.getOriginalFilename());
        byte[] body = multipartFile.getBytes();
        Byte[] bodyByteWrapper = new Byte[body.length];
        for (int i = 0; i < body.length; i++) {
            bodyByteWrapper[i] = body[i];
        }
        dto.setFileBody(bodyByteWrapper);
        AttachmentConverter attachmentConverter = new AttachmentConverterImpl();
        Attachment attachment = addAttachment(attachmentConverter.convertFromDto(dto))
                .orElseThrow(() -> new OperationFailedException(ATTACHMENT_UNABLE_TO_SAVE_MESSAGE));
        OperationResult operationResult = new OperationResult();
        OperationResultConverter resultConverter = new OperationResultConverterImpl();
        operationResult.setId(attachment.getId());
        operationResult.setOperationComplete(true);
        return resultConverter.convertToDto(operationResult);
    }
}
