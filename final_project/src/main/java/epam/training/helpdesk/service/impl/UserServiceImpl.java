package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.exception.authentication.UserNotFoundException;
import epam.training.helpdesk.repository.UserRepository;
import epam.training.helpdesk.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    /**
     * User repository to use.
     */
    private UserRepository userRepository;

    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Integer getUserIdByLoginName(final String loginName) {
        Integer userId;
        User user =
                userRepository.getUserByLogin(loginName).orElseThrow(() ->
                        new UserNotFoundException("Username not found "));
        userId = user.getId();
        return userId;
    }

    @Override
    public User getUserById(Integer id) {
        return userRepository.getUserById(id).orElseThrow(() ->
                new UserNotFoundException("Username not found "));
    }

    @Override
    public List<User> getManagers() {
        return userRepository.getManagers();
    }

    @Override
    public List<User> getEngineers() {
        return userRepository.getEngineers();
    }
}
