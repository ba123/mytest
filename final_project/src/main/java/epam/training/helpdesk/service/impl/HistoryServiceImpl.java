package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.converter.HistoryConverter;
import epam.training.helpdesk.converter.impl.HistoryConverterImpl;
import epam.training.helpdesk.domain.Action;
import epam.training.helpdesk.domain.History;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.history.HistoryDto;
import epam.training.helpdesk.repository.HistoryRepository;
import epam.training.helpdesk.service.HistoryService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class HistoryServiceImpl implements HistoryService {
    private static final Integer DEFAULT_ITEMS_LIMIT = 5;

    private HistoryRepository historyRepository;

    public HistoryServiceImpl(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    public List<HistoryDto> getTicketHistory(final Integer id, String unlimitedMark, Authentication authData) {
        Integer limit = DEFAULT_ITEMS_LIMIT;
        if (unlimitedMark != null && !unlimitedMark.isEmpty()) {
            limit = 0;
        }

        List<History> history = historyRepository.getTicketHistory(id, limit);
        HistoryConverter converter = new HistoryConverterImpl();
        return history.stream()
                .map(converter::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void addHistory(User user, Ticket ticket, Action action, String description) {
        History history = new History();
        history.setActingUser(user);
        history.setDate(new Timestamp(System.currentTimeMillis()));
        history.setHistoryTicket(ticket);
        history.setAction(action.getActionText());
        history.setDescription(action.getActionText() + description);
        historyRepository.addHistory(history);
    }
}
